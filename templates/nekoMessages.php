<?php
    /*
        nekoMessages.php
    */
    switch(base64_decode($_GET["err"]))
    {
        //Errores para Login
        case "x01":
            echo '
            <div class="alert alert-danger">
                <i class="fa fa-info-circle"></i>  <strong>ERROR</strong> Usted no tiene una sesión activa en el sistema, por favor ingrese.
            </div>';
            break;
        case "x02":
            echo '
            <div class="alert alert-danger">
                <i class="fa fa-info-circle"></i>  <strong>ERROR</strong> No pudo pasar el Acceso Seguro al Sistema <i class="fas fa-sad-cry"></i>
            </div>';
            break;
        case "x03":
            echo '
            <div class="alert alert-danger">
                <i class="fa fa-info-circle"></i>  <strong>ERROR</strong> Usuario o Contraseña mal escritos <i class="fas fa-sad-cry"></i>
            </div>';
            break;
        //Mensajes para Workspace
        case "y01":
            echo '
            <div class="alert alert-danger">
                <i class="fa fa-info-circle"></i>  <strong>ERROR</strong> Usted esta intentando acceder a una sección de la cuál no tiene permiso.
            </div>';
            break;
    }
    //Dependiendo del estado del tiquete
    if($neoTicketStatusCode=='9')
    {
        echo '
        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i>  <strong>INFORMACIÓN</strong> Este tiquete ya ha sido resuelto.
        </div>
        ';
    }
?>