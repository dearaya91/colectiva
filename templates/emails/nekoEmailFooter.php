<?php
    /*
      nekoEmailFooter.php
    */
    //HTML
    $nekoEmailFooterHtml='
    <small>&copy; Copyright '.$nekoYearObtained.'. Andalusa Corp S.A. Todos los Derechos Reservados</small>
    <table class="spacer">
       <tbody>
         <tr>
           <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td>
         </tr>
       </tbody>
    </table>
    <center data-parsed="">
       <table align="center" class="menu float-center">
         <tr>
           <td>
             <table>
               <tr>
                 <th colspan="5">Contáctenos por</th>
               </tr>
               <tr>
                 <th class="menu-item float-center"><a href="#" target="_blank">LinkedIn</a></th>
                 <th class="menu-item float-center"><a href="#" target="_blank">Facebook</a></th>
                 <th class="menu-item float-center"><a href="#" target="_blank">Twitter</a></th>
                 <th class="menu-item float-center"><a href="#" target="_blank">Instagram</a></th>
                 <th class="menu-item float-center"><a href="#" target="_blank">Teléfono</a></th>
               </tr>
             </table>
           </td>
         </tr>
       </table>
     </center>
    ';
    //No HTML
    $nekoEmailFooterNoHtml='Copyright '.$nekoYearObtained.'. Andalusa Corp S.A. Todos los Derechos Reservados';
?>