<?php
    /*
    ========================================
    DOCUMENT TITLE: ajaxController.php
    Made by Fenrir
    Content: Omega Ticketer
    ===========================================
    */
    //Encabezado JSON
    header("Content-Type: application/json; charset=UTF-8");
    $neoDocumentControllerAvailable=true;
    //Cuando sea API externa activar
    //include_once("./functions.php");
    include_once("./sessioncontrol.php");
    //Verificar Método
    $neoMethod = $neoRequestMethod;
    //Asignar patrón para verificar API
    $neoRequestPath = explode('/', trim($_SERVER['PATH_INFO'],'/'));
    //Patrón en la tabla
    $neoTypeRequest = $neoRequestPath[0];
    $neoTable = $neoRequestPath[1];
    $neoSubTable = $neoRequestPath[2];
    //Reemplazar
    $neoPageNameBase=$neoTable;
    //Verificar Llave si es que existe
    $neoKey = preg_replace('/[^a-z0-9_]+/i','',array_shift($neoRequestPath));
    //Verificar que solo sea un API Access y no accesible por cliente
    switch($neoMethod)
    {
        case 'POST':
            switch($neoTypeRequest)
            {
                case "DataTables":
                    //Datos Recibidos del GRID
                    $neoRequestData=$_REQUEST;
                    //Array de la Data
                    $neoData = array();
                    //Otro Switch para verificar la tabla de Request
                    switch($neoTable)
                    {
                        case "empresas":
                            //Columnas
                            $neoColumns=
                            array
                            (
                                0 =>'empresas.fechaInicioRelacion',
                                1=>'empresas.nombreempresa',
                                2 =>'empresas.autor',
                                3 =>'estados.estado',
                            );
                            //Buscar
                            if(!empty($neoRequestData['search']['value']))
                            {
                                //Like Title Search>
                                $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                $neoSearchImploded=implode('%',$neoSearchExploded);
                                //Verificar
                                $neoCondition=" WHERE (noticias.fechanoticia LIKE '%$neoSearchImploded%' ";
                                $neoCondition.=" OR noticias.titulonoticia LIKE '%$neoSearchImploded%' ";
                                $neoCondition.=" OR estados.estado LIKE '%$neoSearchImploded%' ";
                                $neoCondition.=" OR noticias.autor LIKE '%$neoSearchImploded%') ";
                            }
                            //Consulta preliminar para contar el número de re,estados.colorgistros
                            $neoMainQueryCounter=$neoConnectDB->Execute("SELECT empresas.codigoempresa,empresas.fechaInicioRelacion,empresas.nombreempresa,empresas.autor,estados.estado,estados.color,estados.fondocolor FROM empresas JOIN estados ON empresas.codigoEstado=estados.codigoEstado $neoCondition ");
                            $neoMainQuery=$neoConnectDB->Execute("SELECT empresas.codigoempresa,empresas.fechaInicioRelacion,empresas.nombreempresa,empresas.autor,estados.estado,estados.color,estados.fondocolor FROM empresas JOIN estados ON empresas.codigoEstado=estados.codigoEstado $neoCondition ORDER BY ". $neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                            //While
                            while($neoDataRow=$neoMainQuery->FetchRow())
                            {
                                //Preparando Array
                                $nestedData=array();
                                //Verificando Datos
                                $neoEnterpriseCode=$neoDataRow["codigoempresa"];
                                $neoEnterpriseDate=$neoDataRow["fechaInicioRelacion"];
                                $neoEnterpriseTitle=$neoDataRow["nombreempresa"];
                                $neoArchiveAuthor=$neoDataRow["autor"];
                                $neoStatusColor=$neoDataRow["color"];
                                $neoStatus=$neoDataRow["estado"];
                                //Guardando en Array
                                $nestedData[] = $neoEnterpriseDate;
                                $nestedData[] = $neoEnterpriseTitle;
                                $nestedData[] = $neoArchiveAuthor;
                                //Para Botones si es que se Necesita
                                $nestedData[] = '<p style="text-align:center;"><a class="btn btn-success" onclick="window.open(\'./enterpriseCreation.php?neoEnterprise='.base64_encode($neoEnterpriseCode).'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                $neoData[] = $nestedData;
                            }
                            //Fin
                            break;
                        case "usuarios":
                            //Columnas
                            $neoColumns=
                            array
                            (
                                0 =>'usuarios.nombreusuario',
                                1=>'usuarios.correo',
                                2 =>'estados.estado',
                            );
                            //Buscar
                            if(!empty($neoRequestData['search']['value']))
                            {
                                //Like Title Search>
                                $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                $neoSearchImploded=implode('%',$neoSearchExploded);
                                //Verificar
                                $neoCondition=" WHERE (usuarios.nombreusuario LIKE '%$neoSearchImploded%' ";
                                $neoCondition.=" OR usuarios.correo LIKE '%$neoSearchImploded%' ";
                                $neoCondition.=" OR estados.estado LIKE '%$neoSearchImploded%') ";
                            }
                            //Consulta preliminar para contar el número de re,estados.colorgistros
                            $neoMainQueryCounter=$neoConnectDB->Execute("SELECT * FROM usuarios JOIN estados ON usuarios.codigoEstado=estados.codigoEstado $neoCondition ");
                            $neoMainQuery=$neoConnectDB->Execute("SELECT * FROM usuarios JOIN estados ON usuarios.codigoEstado=estados.codigoEstado $neoCondition ORDER BY ". $neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                            //While
                            while($neoDataRow=$neoMainQuery->FetchRow())
                            {
                                //Preparando Array
                                $nestedData=array();
                                //Verificando Datos
                                $neoUser=$neoDataRow["codigousuario"];
                                $neoUserName=$neoDataRow["nombreusuario"];
                                $neoUserEmailAddress=$neoDataRow["correo"];
                                $neoArchiveAuthor=$neoDataRow["autor"];
                                $neoStatusColor=$neoDataRow["color"];
                                $neoStatus=$neoDataRow["estado"];
                                $neoStatusColor=$neoDataRow["color"];
                                $neoStatusBG=$neoDataRow["fondocolor"];
                                //Guardando en Array
                                $nestedData[] = $neoUserName;
                                $nestedData[] = $neoUserEmailAddress;
                                $nestedData[] = '<span class="version status" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoStatus.'</span>';
                                //Para Botones si es que se Necesita
                                $nestedData[] = '<p style="text-align:center;"><a class="btn btn-success" onclick="window.open(\'./userManagement.php?neoUser='.base64_encode($neoUser).'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                $neoData[] = $nestedData;
                            }
                            //Fin
                            break;
                        case "noticias":
                            switch($neoSubTable)
                            {
                                case "01":
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'noticias.fechanoticia',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search>
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (noticias.fechanoticia LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR noticias.titulonoticia LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR noticias.autor LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT noticias.codigonoticia,noticias.fechanoticia,noticias.titulonoticia,estados.estado,estados.color,estados.fondocolor,noticias.contenidonoticia,noticias.codigoarchivo,noticias.autor,noticias.codigoTipoNoticia FROM noticias JOIN estados ON noticias.codigoEstado=estados.codigoEstado WHERE noticias.codigoEstado='6' $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT noticias.codigonoticia,noticias.fechanoticia,noticias.titulonoticia,estados.estado,estados.color,estados.fondocolor,noticias.contenidonoticia,noticias.codigoarchivo,noticias.autor,noticias.codigoTipoNoticia FROM noticias JOIN estados ON noticias.codigoEstado=estados.codigoEstado WHERE noticias.codigoEstado='6' $neoCondition ORDER BY ". $neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoNoticeCode=$neoDataRow["codigonoticia"];
                                        $neoNoticeDate=$neoDataRow["fechanoticia"];
                                        $neoNoticeTitle=$neoDataRow["titulonoticia"];
                                        $neoNoticeContent=$neoDataRow["contenidonoticia"];
                                        $neoArchiveCode=$neoDataRow["codigoarchivo"];
                                        $neoNoticeTypeCode=$neoDataRow["codigoTipoNoticia"];
                                        $neoNoticeType=$neoConnectDB->GetOne("SELECT denominacion FROM condicionadores WHERE codigocondicionador='$neoNoticeTypeCode' ");
                                        $neoArchiveAuthor=$neoDataRow["autor"];
                                        $neoStatusColor=$neoDataRow["color"];
                                        $neoStatusBG=$neoDataRow["fondocolor"];
                                        $neoStatus=$neoDataRow["estado"];
                                        $neoTableContentFooter='';
                                        //Verificar si hay archivo
                                        if($neoArchiveCode!="0" AND !empty($neoArchiveCode))
                                        {
                                            $neoFileName=$neoConnectDB->GetOne("SELECT nombrearchivo FROM archivos WHERE codigoarchivo='$neoArchiveCode' ");
                                            $neoTableContentFooter='
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3">
                                                        <a href="./repository/notices/'.$neoFileName.'" target="_blank">Descargar Archivo</a>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                            ';
                                        }
                                        //Formateando
                                        $neoContent='
                                            <table style="width:100%;" border="0">
                                            <thead>
                                                    <tr>
                                                        <th style="width:25%;vertical-align:middle;">
                                                        <span style="background-color:#00a260;color:'.$neoStatusColor.';" class="version status">Autor: '.$neoArchiveAuthor.'</span>
                                                        </th>
                                                        <th style="width:50%;text-align:center;">
                                                            <h1>'.$neoNoticeTitle.'</h1>
                                                        </th>
                                                        <th style="width:25%;vertical-align:middle;text-align:right;">
                                                            <span style="background-color:#000;color:'.$neoStatusColor.';" class="version status">Fecha Publicación: '.$neoNoticeDate.'</span>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="vertical-align:middle;text-align:right;" colspan="3">
                                                        <span style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';" class="version status">'.$neoNoticeType.'</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            '.$neoNoticeContent.'
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                '.$neoTableContentFooter.'
                                            </table>
                                        ';
                                        //Guardando en Array
                                        $nestedData[] = $neoContent;
                                        $neoData[] = $nestedData;
                                    }
                                    //Fin
                                    break;
                                case "02":
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'noticias.fechanoticia',
                                        1=>'noticias.titulonoticia',
                                        2 =>'noticias.autor',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search>
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" WHERE (noticias.fechanoticia LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR noticias.titulonoticia LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR noticias.autor LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de re,estados.colorgistros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT noticias.codigonoticia,noticias.fechanoticia,noticias.titulonoticia,estados.estado,estados.color,estados.fondocolor,noticias.contenidonoticia,noticias.codigoarchivo,noticias.autor FROM noticias JOIN estados ON noticias.codigoEstado=estados.codigoEstado $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT noticias.codigonoticia,noticias.fechanoticia,noticias.titulonoticia,estados.estado,estados.color,estados.fondocolor,noticias.contenidonoticia,noticias.codigoarchivo,noticias.autor FROM noticias JOIN estados ON noticias.codigoEstado=estados.codigoEstado $neoCondition ORDER BY ". $neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoNoticeCode=$neoDataRow["codigonoticia"];
                                        $neoNoticeDate=$neoDataRow["fechanoticia"];
                                        $neoNoticeTitle=$neoDataRow["titulonoticia"];
                                        $neoNoticeContent=$neoDataRow["contenidonoticia"];
                                        $neoArchiveCode=$neoDataRow["codigoarchivo"];
                                        $neoArchiveAuthor=$neoDataRow["autor"];
                                        $neoStatusColor=$neoDataRow["color"];
                                        $neoStatus=$neoDataRow["estado"];
                                        $neoTableContentFooter='';
                                        //Guardando en Array
                                        $nestedData[] = $neoNoticeDate;
                                        $nestedData[] = $neoNoticeTitle;
                                        $nestedData[] = $neoArchiveAuthor;
                                        //Para Botones si es que se Necesita
                                        $nestedData[] = '<p style="text-align:center;"><a class="btn btn-success" onclick="window.open(\'./noticesCreate.php?neoNotice='.base64_encode($neoNoticeCode).'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                        $neoData[] = $nestedData;
                                    }
                                    //Fin
                                    break;
                            }
                            //Fin
                            break;
                        case 'documentos':
                            //Dependiendo del Caso es Otra Consulta
                            switch($neoSubTable)
                            {
                                case "07":
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'documentosAsignados.nombreDocumento',
                                        1 =>'archivos.fechasubido',
                                        2 =>'archivos.autor',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" WHERE (documentosAsignados.nombreDocumento LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR archivos.fechasubido LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR archivos.autor LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT documentosAsignados.codigoDocumentoAsignado,documentosAsignados.nombreDocumento,archivos.fechasubido,archivos.autor  FROM documentosAsignados JOIN archivos ON documentosAsignados.codigoarchivo=archivos.codigoarchivo $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT documentosAsignados.codigoDocumentoAsignado,documentosAsignados.nombreDocumento,archivos.fechasubido,archivos.autor  FROM documentosAsignados JOIN archivos ON documentosAsignados.codigoarchivo=archivos.codigoarchivo $neoCondition ORDER BY ". $neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoDocumentAssignedNumber=$neoDataRow["codigoDocumentoAsignado"];
                                        $neoDocumentName=$neoDataRow["nombreDocumento"];
                                        $neoDocumentDate=$neoDataRow["fechasubido"];
                                        $neoArchiveCode=$neoDataRow["codigoarchivo"];
                                        $neoArchiveAuthor=$neoDataRow["autor"];
                                        //Guardando en Array
                                        $nestedData[] = $neoDocumentName;
                                        $nestedData[] = $neoDocumentDate;
                                        $nestedData[] = $neoArchiveAuthor;
                                        $nestedData[] = '<p style="text-align:center;"><a class="btn btn-sm btn-success" onclick="window.open(\'./documentsAdder.php?neoDocument='.base64_encode($neoDocumentAssignedNumber).'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                default:
                                    //Dependiendo de la Subtabla Principal
                                    switch($neoSubTable)
                                    {
                                        case "01": //MANUALES
                                            $neoMainCondition='AND documentosAsignados.tipoDocumento="MANUALES"';
                                            break;
                                        case "02": //LICENCIAS
                                            $neoMainCondition='AND documentosAsignados.tipoDocumento="LICENCIAS"';
                                            break;
                                        case "03": //GUÍAS
                                            $neoMainCondition='AND documentosAsignados.tipoDocumento="GUÍAS"';
                                            break;
                                        case "04": //FACTURAS
                                            $neoMainCondition='AND documentosAsignados.tipoDocumento="FACTURAS"';
                                            break;
                                        case "05": //CONTRATOS
                                            $neoMainCondition='AND documentosAsignados.tipoDocumento="CONTRATOS"';
                                            break;
                                        case "06": //COTIZACIONES
                                            $neoMainCondition='AND documentosAsignados.tipoDocumento="COTIZACIONES"';
                                            break;
                                    }
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'documentosAsignados.nombreDocumento',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (documentosAsignados.nombreDocumento LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR archivos.fechasubido LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR archivos.autor LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT documentosAsignados.nombreDocumento,archivos.fechasubido,archivos.autor  FROM documentosAsignados JOIN archivos ON documentosAsignados.codigoarchivo=archivos.codigoarchivo WHERE documentosAsignados.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoMainCondition $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT documentosAsignados.nombreDocumento,archivos.fechasubido,archivos.autor  FROM documentosAsignados JOIN archivos ON documentosAsignados.codigoarchivo=archivos.codigoarchivo WHERE documentosAsignados.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoMainCondition $neoCondition ORDER BY ". $neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoDocumentName=$neoDataRow["nombreDocumento"];
                                        $neoDocumentDate=$neoDataRow["fechasubido"];
                                        $neoArchiveCode=$neoDataRow["codigoarchivo"];
                                        $neoArchiveAuthor=$neoDataRow["autor"];
                                        //Archivo
                                        $neoFileName=$neoConnectDB->GetOne("SELECT nombrearchivo FROM archivos WHERE codigoarchivo='$neoArchiveCode' ");
                                        $neoTableContentFooter='
                                        <tfoot>
                                            <tr>
                                                <td colspan="3">
                                                    <a href="./repository/documents/'.$neoFileName.'" target="_blank">Descargar Archivo</a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        ';
                                        //Formateando
                                        $neoContent='
                                            <table style="width:100%;" border="0">
                                            <thead>
                                                    <tr>
                                                        <th style="width:25%;vertical-align:middle;">
                                                            Autor: '.$neoDocumentName.'
                                                        </th>
                                                        <th style="width:50%;text-align:center;">
                                                            <i class="fa fa-file-o"></i>&nbsp;<span>'.$neoNoticeTitle.'<span>
                                                        </th>
                                                        <th style="width:25%;vertical-align:middle;text-align:right;">
                                                            Fecha Subido: '.$neoDocumentDate.'
                                                        </th>
                                                    </tr>
                                                </thead>
                                                '.$neoTableContentFooter.'
                                            </table>
                                        ';
                                        //Guardando en Array
                                        $nestedData[] = $neoContent;
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                            }
                            //Fin
                            break;
                        case 'tiquetes':
                            //Switch para el tipo de Tabla
                            switch($neoSubTable)
                            {
                                case "01": //Vigentes
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'calendario.fechainicio',
                                        1 =>'calendario.asunto',
                                        2 =>'calendario.numerotiquete',
                                        3 =>'calendario.fechavencimiento',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (calendario.fechainicio LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.asunto LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.numerotiquete LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechavencimiento LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    if($_SESSION["neoUserRoleCode"]!="4")//Si es parte del Staff este podra ver todos los tiquetes
                                    {
                                        //Consulta preliminar para contar el número de registros
                                        $neoMainQueryCounter=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento>='$neoCurrentDateTime' $neoCondition ");
                                        $neoMainQuery=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.codigoEstado,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento>='$neoCurrentDateTime' $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    }
                                    else //Todos los demas deben ver lo de ellos solamente
                                    {
                                        //Consulta preliminar para contar el número de registros
                                        $neoMainQueryCounter=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento>='$neoCurrentDateTime' AND calendario.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition ");
                                        $neoMainQuery=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.codigoEstado,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento>='$neoCurrentDateTime' AND calendario.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    }
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoTicketCode=$neoDataRow["codigotiquete"];
                                        $neoPriorityCode=$neoDataRow["codigoprioridad"];
                                        $neoTicketDate=$neoDataRow["fechainicio"];
                                        $neoTicketSubject=$neoDataRow["asunto"];
                                        $neoTicketNumber=$neoDataRow["numerotiquete"];
                                        $neoTicketEndingDate=$neoDataRow["fechavencimiento"];
                                        $neoTicketStatus=$neoDataRow["estado"];
                                        $neoTicketStatusCode=$neoDataRow["codigoEstado"];
                                        //GetColors
                                        $neoStatusColorsSql=$neoConnectDB->Execute("SELECT fondocolor,color FROM estados WHERE codigoEstado='$neoTicketStatusCode' ");
                                        while($neoStatusColorsData=$neoStatusColorsSql->FetchRow())
                                        {
                                            //Data
                                            $neoStatusBG=$neoStatusColorsData["fondocolor"];
                                            $neoStatusColor=$neoStatusColorsData["color"];
                                        }
                                        //Contenido
                                        //Guardando en Array
                                        $nestedData[] = $neoTicketDate;
                                        $nestedData[] = $neoTicketSubject;
                                        $nestedData[] = $neoTicketNumber;
                                        $nestedData[] = $neoTicketEndingDate;
                                        $nestedData[] = '<span class="version status" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoTicketStatus.'</span>';
                                        //Para Botones si es que se Necesita
                                        //$nestedData[] = '<p style="text-align:center;"><a class="btn btn-sm btn-success" onclick="window.open(\'./'.$_POST["getform"].'?getlanguage='.$getlanguage.'&getnur='.base64_encode($row["nur"]).'&getoperation='.$getoperation.'&getsection='.$getsection.'&getcomponent='.$getcomponent.'&getgroupmenu='.$getgroupmenu.'&getmenu='.$getmenu.'&getsubmenu='.$getsubmenu.'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                case "02": //Vencidos
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'calendario.fechainicio',
                                        1 =>'calendario.asunto',
                                        2 =>'calendario.numerotiquete',
                                        3 =>'calendario.fechavencimiento',
                                        4 =>'calendario.usuariosSoporte',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (calendario.fechainicio LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.asunto LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.numerotiquete LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.usuariosSoporte LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechavencimiento LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    if($_SESSION["neoUserRoleCode"]!="4")//Si es parte del Staff este podra ver todos los tiquetes
                                    {
                                        $neoMainQueryCounter=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento<='$neoCurrentDateTime' AND calendario.codigoEstado!='9' $neoCondition  ");
                                        $neoMainQuery=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.codigoEstado,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento<='$neoCurrentDateTime' AND calendario.codigoEstado!='9' $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    }
                                    else //Las demas solo deben ver los de ellos solamente
                                    {
                                        $neoMainQueryCounter=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento<='$neoCurrentDateTime' AND calendario.codigoEstado!='9' AND calendario.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition  ");
                                        $neoMainQuery=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.codigoEstado,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechavencimiento,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.fechavencimiento<='$neoCurrentDateTime' AND calendario.codigoEstado!='9' AND calendario.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    }
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoTicketCode=$neoDataRow["codigotiquete"];
                                        $neoPriorityCode=$neoDataRow["codigoprioridad"];
                                        $neoTicketDate=$neoDataRow["fechainicio"];
                                        $neoTicketSubject=$neoDataRow["asunto"];
                                        $neoTicketNumber=$neoDataRow["numerotiquete"];
                                        $neoTicketEndingDate=$neoDataRow["fechavencimiento"];
                                        $neoTicketStatus=$neoDataRow["estado"];
                                        $neoTicketStatusCode=$neoDataRow["codigoEstado"];
                                        //GetColors
                                        $neoStatusColorsSql=$neoConnectDB->Execute("SELECT fondocolor,color FROM estados WHERE codigoEstado='$neoTicketStatusCode' ");
                                        while($neoStatusColorsData=$neoStatusColorsSql->FetchRow())
                                        {
                                            //Data
                                            $neoStatusBG=$neoStatusColorsData["fondocolor"];
                                            $neoStatusColor=$neoStatusColorsData["color"];
                                        }
                                        $neoTicketSupportUsers=$neoDataRow["usuariosSoporte"];
                                        //Array Explode
                                        $neoTicketUserSupportNamesArray=array();
                                        $neoTicketSupportArray=explode(",",$neoTicketSupportUsers);
                                        foreach($neoTicketSupportArray as $neoKey => $neoValue)
                                        {
                                            $neoTicketUserSupportNamesArray[]=$neoConnectDB->GetOne("SELECT nombreusuario FROM usuarios WHERE codigousuario='$neoValue' ");
                                        }
                                        //Array Implode
                                        $neoTicketUserSupportNames=implode(', ',$neoTicketUserSupportNamesArray);
                                        //Guardando en Array
                                        $nestedData[] = $neoTicketDate;
                                        $nestedData[] = $neoTicketSubject;
                                        $nestedData[] = $neoTicketNumber;
                                        $nestedData[] = $neoTicketEndingDate;
                                        $nestedData[] = $neoTicketUserSupportNames;
                                        $nestedData[] = '<span class="version status" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoTicketStatus.'</span>';
                                        //Para Botones si es que se Necesita
                                        //$nestedData[] = '<p style="text-align:center;"><a class="btn btn-sm btn-success" onclick="window.open(\'./'.$_POST["getform"].'?getlanguage='.$getlanguage.'&getnur='.base64_encode($row["nur"]).'&getoperation='.$getoperation.'&getsection='.$getsection.'&getcomponent='.$getcomponent.'&getgroupmenu='.$getgroupmenu.'&getmenu='.$getmenu.'&getsubmenu='.$getsubmenu.'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                case "03": //Resueltos
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'calendario.fechainicio',
                                        1 =>'calendario.asunto',
                                        2 =>'calendario.numerotiquete',
                                        3 =>'calendario.fechafin',
                                        4 =>'calendario.usuariosSoporte',
                                    );
                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (calendario.fechainicio LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.asunto LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.numerotiquete LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.usuariosSoporte LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechafin LIKE '%$neoSearchImploded%') ";
                                    }
                                    if($_SESSION["neoUserRoleCode"]!="4")//Si es parte del Staff este podra ver todos los tiquetes
                                    {
                                        //Consulta preliminar para contar el número de registros
                                        $neoMainQueryCounter=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechafin,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.codigoEstado='9' AND calendario.fechavencimiento<='$neoCurrentDateTime' $neoCondition  ");
                                        $neoMainQuery=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.codigoEstado,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechafin,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.codigoEstado='9' AND calendario.fechavencimiento<='$neoCurrentDateTime' $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    }
                                    else //Todos los demas deben ver lo de ellos solamente
                                    {
                                        //Consulta preliminar para contar el número de registros
                                        $neoMainQueryCounter=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechafin,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.codigoEstado='9' AND calendario.fechavencimiento<='$neoCurrentDateTime' AND calendario.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition  ");
                                        $neoMainQuery=$neoConnectDB->Execute("SELECT calendario.codigotiquete,calendario.codigoEstado,calendario.usuariosSoporte,calendario.fechainicio,calendario.asunto,calendario.numerotiquete,calendario.fechafin,calendario.estado,calendario.fondoPrioridad FROM calendario WHERE calendario.codigoEstado='9' AND calendario.fechavencimiento<='$neoCurrentDateTime' AND calendario.codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    }
                                    //While
                                    while($neoDataRow=$neoMainQuery->FetchRow())
                                    {
                                        //Preparando Array
                                        $nestedData=array();
                                        //Verificando Datos
                                        $neoTicketCode=$neoDataRow["codigotiquete"];
                                        $neoPriorityCode=$neoDataRow["codigoprioridad"];
                                        $neoTicketDate=$neoDataRow["fechainicio"];
                                        $neoTicketSubject=$neoDataRow["asunto"];
                                        $neoTicketNumber=$neoDataRow["numerotiquete"];
                                        $neoTicketEndingDate=$neoDataRow["fechafin"];
                                        $neoTicketSupportUsers=$neoDataRow["usuariosSoporte"];
                                        $neoTicketStatus=$neoDataRow["estado"];
                                        $neoTicketStatusCode=$neoDataRow["codigoEstado"];
                                        //GetColors
                                        $neoStatusColorsSql=$neoConnectDB->Execute("SELECT fondocolor,color FROM estados WHERE codigoEstado='$neoTicketStatusCode' ");
                                        while($neoStatusColorsData=$neoStatusColorsSql->FetchRow())
                                        {
                                            //Data
                                            $neoStatusBG=$neoStatusColorsData["fondocolor"];
                                            $neoStatusColor=$neoStatusColorsData["color"];
                                        }
                                        //Array Explode
                                        $neoTicketUserSupportNamesArray=array();
                                        $neoTicketSupportArray=explode(",",$neoTicketSupportUsers);
                                        foreach($neoTicketSupportArray as $neoKey => $neoValue)
                                        {
                                            $neoTicketUserSupportNamesArray[]=$neoConnectDB->GetOne("SELECT nombreusuario FROM usuarios WHERE codigousuario='$neoValue' ");
                                        }
                                        //Array Implode
                                        $neoTicketUserSupportNames=implode(', ',$neoTicketUserSupportNamesArray);
                                        //Guardando en Array
                                        $nestedData[] = $neoTicketDate;
                                        $nestedData[] = $neoTicketSubject;
                                        $nestedData[] = $neoTicketNumber;
                                        $nestedData[] = $neoTicketEndingDate;
                                        $nestedData[] = $neoTicketUserSupportNames;
                                        $nestedData[] = '<span class="version status" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoTicketStatus.'</span>';
                                        //Para Botones si es que se Necesita
                                        //$nestedData[] = '<p style="text-align:center;"><a class="btn btn-sm btn-success" onclick="window.open(\'./'.$_POST["getform"].'?getlanguage='.$getlanguage.'&getnur='.base64_encode($row["nur"]).'&getoperation='.$getoperation.'&getsection='.$getsection.'&getcomponent='.$getcomponent.'&getgroupmenu='.$getgroupmenu.'&getmenu='.$getmenu.'&getsubmenu='.$getsubmenu.'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                case "04": //Tiquetes de Soporte en Discusión
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'calendario.fechainicio',
                                    );

                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (calendario.numerotiquete LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.asunto LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechainicio LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechavencimiento LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT * FROM `calendario` WHERE codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT * FROM `calendario` WHERE codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' $neoCondition ORDER BY codigoprioridad DESC LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    if($neoMainQuery->RecordCount() > 0)
                                    {
                                        //While
                                        while($neoDataRow=$neoMainQuery->FetchRow())
                                        {
                                            //Preparando Array
                                            $nestedData=array();
                                            $neoTicketCode=base64_encode($neoDataRow["codigotiquete"]);
                                            $neoTicketNumber=$neoDataRow["numerotiquete"];
                                            $neoTicketSubject=$neoDataRow["asunto"];
                                            $neoPriorityBG=$neoDataRow["fondoPrioridad"];
                                            $neoPriorityTextColor=$neoDataRow["colorTexto"];
                                            $neoPriorityText=$neoDataRow["prioridad"];
                                            $neoTicketBeginDate=$neoDataRow["fechainicio"];
                                            $neoTicketExpirationDate=$neoDataRow["fechavencimiento"];
                                            $neoTicketEndDate=$neoDataRow["fechafin"];
                                            $neoTicketMessage=$neoDataRow["mensajeTiquete"];
                                            $neoTicketStatus=$neoDataRow["estado"];
                                            $neoTicketStatusCode=$neoDataRow["codigoEstado"];
                                            //GetColors
                                            $neoStatusColorsSql=$neoConnectDB->Execute("SELECT fondocolor,color FROM estados WHERE codigoEstado='$neoTicketStatusCode' ");
                                            while($neoStatusColorsData=$neoStatusColorsSql->FetchRow())
                                            {
                                                //Data
                                                $neoStatusBG=$neoStatusColorsData["fondocolor"];
                                                $neoStatusColor=$neoStatusColorsData["color"];
                                            }
                                            //Contenido
                                            $neoTicketsContent='
                                            <tr>
                                                <td>
                                                    <!-- ACORDION -->
                                                    <div id="neoMainAccordion'.$neoTicketNumber.'">
                                                        <!-- TIQUETES -->
                                                        <div class="card border-primary">
                                                            <div class="card-header text-primary" id="neoHeadingTicket'.$neoTicketNumber.'" class="btn-link" data-toggle="collapse" data-target="#neoTicket'.$neoTicketNumber.'" aria-expanded="true" aria-controls="neoTicket'.$neoTicketNumber.'">
                                                                <h4><strong>'.$neoTicketNumber.' - '.$neoTicketSubject.' <span class="version status pull-right" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoTicketStatus.'</span></strong></h4>
                                                            </div>
                                                            <!-- CONTENIDO DE COLAPSO -->
                                                            <div id="neoTicket'.$neoTicketNumber.'" class="collapse" aria-labelledby="neoHeadingTicket'.$neoTicketNumber.'" data-parent="#neoMainAccordion'.$neoTicketNumber.'">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Prioridad: <span class="version status" style="background-color:'.$neoPriorityBG.';color:'.$neoPriorityTextColor.';">'.$neoPriorityText.'</span></h4>
                                                                    <hr>
                                                                    <p class="card-text"><span class="pull-left">Fecha de Inicio: '.$neoTicketBeginDate.'</span> <span class="pull-right">Fecha de Vencimiento: '.$neoTicketExpirationDate.'</span></p>
                                                                    <br>
                                                                    <p class="card-text text-primary">'.$neoTicketMessage.'</p>
                                                                    <br>
                                                                    <a class="btn btn-lg btn-info" href="discussion.php?neoTicket='.$neoTicketCode.'">Ver Tiquete</a>
                                                                </div>
                                                            </div>
                                                            <!-- CONTENIDO DE COLAPSO -->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <!-- TIQUETES -->
                                                    </div>
                                                    <!-- ACORDION -->
                                                </tr>
                                            </td>
                                            ';
                                            $nestedData[] = $neoTicketsContent;
                                            $neoData[] = $nestedData;
                                        }
                                    }
                                    else
                                    {
                                        //Array
                                        $nestedData=array();
                                        //Content
                                        $neoTicketsContent = '
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <p class="text-warning-messages">NO EXISTEN TIQUETES GENERADOS</p>
                                                </div>
                                            </td>
                                        </tr>
                                        ';
                                        //Data
                                        $nestedData[] = $neoTicketsContent;
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                case "05": //Tiquetes de Soporte en Discusión Staff
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'calendario.fechainicio',
                                    );

                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" AND (calendario.numerotiquete LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.asunto LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechainicio LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.nombreempresa LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechavencimiento LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT * FROM `calendario` WHERE usuariosSoporte LIKE '%".$_SESSION["neoUserCode"]."%' $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT * FROM `calendario` WHERE usuariosSoporte LIKE '%".$_SESSION["neoUserCode"]."%' $neoCondition ORDER BY codigoprioridad DESC LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    if($neoMainQuery->RecordCount() > 0)
                                    {
                                        //While
                                        while($neoDataRow=$neoMainQuery->FetchRow())
                                        {
                                            //Preparando Array
                                            $nestedData=array();
                                            $neoTicketCode=base64_encode($neoDataRow["codigotiquete"]);
                                            $neoTicketNumber=$neoDataRow["numerotiquete"];
                                            $neoTicketSubject=$neoDataRow["asunto"];
                                            $neoPriorityBG=$neoDataRow["fondoPrioridad"];
                                            $neoPriorityTextColor=$neoDataRow["colorTexto"];
                                            $neoTicketEnterpriseName=$neoDataRow["nombreempresa"];
                                            $neoPriorityText=$neoDataRow["prioridad"];
                                            $neoTicketBeginDate=$neoDataRow["fechainicio"];
                                            $neoTicketExpirationDate=$neoDataRow["fechavencimiento"];
                                            $neoTicketEndDate=$neoDataRow["fechafin"];
                                            $neoTicketMessage=$neoDataRow["mensajeTiquete"];
                                            $neoTicketStatus=$neoDataRow["estado"];
                                            $neoTicketStatusCode=$neoDataRow["codigoEstado"];
                                            //GetColors
                                            $neoStatusColorsSql=$neoConnectDB->Execute("SELECT fondocolor,color FROM estados WHERE codigoEstado='$neoTicketStatusCode' ");
                                            while($neoStatusColorsData=$neoStatusColorsSql->FetchRow())
                                            {
                                                //Data
                                                $neoStatusBG=$neoStatusColorsData["fondocolor"];
                                                $neoStatusColor=$neoStatusColorsData["color"];
                                            }
                                            //Contenido
                                            $neoTicketsContent='
                                            <tr>
                                                <td>
                                                    <!-- ACORDION -->
                                                    <div id="neoMainAccordion'.$neoTicketNumber.'">
                                                        <!-- TIQUETES -->
                                                        <div class="card border-primary">
                                                            <div class="card-header text-primary" id="neoHeadingTicket'.$neoTicketNumber.'" class="btn-link" data-toggle="collapse" data-target="#neoTicket'.$neoTicketNumber.'" aria-expanded="true" aria-controls="neoTicket'.$neoTicketNumber.'">
                                                                <h4><strong>'.$neoTicketNumber.' - '.$neoTicketSubject.' - SOLICITADO POR: '.$neoTicketEnterpriseName.' <span class="version status pull-right" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoTicketStatus.'</span></strong></h4>
                                                            </div>
                                                            <!-- CONTENIDO DE COLAPSO -->
                                                            <div id="neoTicket'.$neoTicketNumber.'" class="collapse" aria-labelledby="neoHeadingTicket'.$neoTicketNumber.'" data-parent="#neoMainAccordion'.$neoTicketNumber.'">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Prioridad: <span class="version status" style="background-color:'.$neoPriorityBG.';color:'.$neoPriorityTextColor.';">'.$neoPriorityText.'</span></h4>
                                                                    <hr>
                                                                    <p class="card-text"><span class="pull-left">Fecha de Inicio: '.$neoTicketBeginDate.'</span> <span class="pull-right">Fecha de Vencimiento: '.$neoTicketExpirationDate.'</span></p>
                                                                    <br>
                                                                    <p class="card-text text-primary">'.$neoTicketMessage.'</p>
                                                                    <br>
                                                                    <a class="btn btn-lg btn-info" href="discussionStaff.php?neoTicket='.$neoTicketCode.'">Ver Tiquete</a>
                                                                </div>
                                                            </div>
                                                            <!-- CONTENIDO DE COLAPSO -->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <!-- TIQUETES -->
                                                    </div>
                                                    <!-- ACORDION -->
                                                </tr>
                                            </td>
                                            ';
                                            $nestedData[] = $neoTicketsContent;
                                            $neoData[] = $nestedData;
                                        }
                                    }
                                    else
                                    {
                                        //Array
                                        $nestedData=array();
                                        //Content
                                        $neoTicketsContent = '
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <p class="text-warning-messages">NO EXISTEN TIQUETES GENERADOS</p>
                                                </div>
                                            </td>
                                        </tr>
                                        ';
                                        //Data
                                        $nestedData[] = $neoTicketsContent;
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                case "06": //Tiquetes de Soporte Asignables
                                    //Columnas
                                    $neoColumns=
                                    array
                                    (
                                        0 =>'calendario.numerotiquete',
                                        1 =>'calendario.fechainicio',
                                        2 =>'calendario.asunto',
                                        3 =>'calendario.solicitante',
                                        4 =>'calendario.nombreempresa',
                                        5 =>'calendario.estado',
                                    );

                                    //Buscar
                                    if(!empty($neoRequestData['search']['value']))
                                    {
                                        //Like Title Search
                                        $neoSearchExploded=explode(' ',($neoRequestData['search']['value']));
                                        $neoSearchImploded=implode('%',$neoSearchExploded);
                                        //Verificar
                                        $neoCondition=" WHERE (calendario.numerotiquete LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.asunto LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.fechainicio LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.nombreempresa LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.estado LIKE '%$neoSearchImploded%' ";
                                        $neoCondition.=" OR calendario.solicitante LIKE '%$neoSearchImploded%') ";
                                    }
                                    //Consulta preliminar para contar el número de registros
                                    $neoMainQueryCounter=$neoConnectDB->Execute("SELECT * FROM `calendario` $neoCondition ");
                                    $neoMainQuery=$neoConnectDB->Execute("SELECT * FROM `calendario` $neoCondition ORDER BY ".$neoColumns[$neoRequestData['order'][0]['column']]." ".$neoRequestData['order'][0]['dir']." LIMIT ".$neoRequestData['start']." ,".$neoRequestData['length']." ");
                                    if($neoMainQuery->RecordCount() > 0)
                                    {
                                        //While
                                        while($neoDataRow=$neoMainQuery->FetchRow())
                                        {
                                            //Preparando Array
                                            $nestedData=array();
                                            $neoTicketCode=base64_encode($neoDataRow["codigotiquete"]);
                                            $neoTicketNumber=$neoDataRow["numerotiquete"];
                                            $neoTicketSubject=$neoDataRow["asunto"];
                                            $neoPriorityBG=$neoDataRow["fondoPrioridad"];
                                            $neoPriorityTextColor=$neoDataRow["colorTexto"];
                                            $neoTicketEnterpriseName=$neoDataRow["nombreempresa"];
                                            $neoPriorityText=$neoDataRow["prioridad"];
                                            $neoTicketBeginDate=$neoDataRow["fechainicio"];
                                            $neoTicketExpirationDate=$neoDataRow["fechavencimiento"];
                                            $neoTicketEndDate=$neoDataRow["fechafin"];
                                            $neoTicketRequester=$neoDataRow["solicitante"];
                                            $neoTicketEnterpriseName=$neoDataRow["nombreempresa"];
                                            $neoTicketStatus=$neoDataRow["estado"];
                                            $neoTicketStatusCode=$neoDataRow["codigoEstado"];
                                            //GetColors
                                            $neoStatusColorsSql=$neoConnectDB->Execute("SELECT fondocolor,color FROM estados WHERE codigoEstado='$neoTicketStatusCode' ");
                                            while($neoStatusColorsData=$neoStatusColorsSql->FetchRow())
                                            {
                                                //Data
                                                $neoStatusBG=$neoStatusColorsData["fondocolor"];
                                                $neoStatusColor=$neoStatusColorsData["color"];
                                            }
                                            //Contenido
                                            $nestedData[] = $neoTicketNumber;
                                            $nestedData[] = $neoTicketBeginDate;
                                            $nestedData[] = $neoTicketSubject;
                                            $nestedData[] = $neoTicketRequester;
                                            $nestedData[] = $neoTicketEnterpriseName;
                                            $nestedData[] = '<span class="version status" style="background-color:'.$neoStatusBG.';color:'.$neoStatusColor.';">'.$neoTicketStatus.'</span>';
                                            //Button
                                            $nestedData[] = '<p style="text-align:center;"><a class="btn btn-sm btn-success" onclick="window.open(\'./ticketAssignation.php?neoTicket='.($neoTicketCode).'\',\'_top\')" title="Edita el registro seleccionado"><i class="fa fa-pencil"></i>&nbsp;Editar</a></p>';
                                            $neoData[] = $nestedData;
                                        }
                                    }
                                    else
                                    {
                                        //Array
                                        $nestedData=array();
                                        //Content
                                        $neoTicketsContent = '
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <p class="text-warning-messages">NO EXISTEN TIQUETES GENERADOS</p>
                                                </div>
                                            </td>
                                        </tr>
                                        ';
                                        //Data
                                        $nestedData[] = $neoTicketsContent;
                                        $neoData[] = $nestedData;
                                    }
                                    break;
                                default:
                                    header($_SERVER['SERVER_PROTOCOL'] .'HTTP/1.1 401 Unauthorized', true, 401);
                                    die;
                                    break;
                            }
                            //Fin
                            break;
                        default:
                            header($_SERVER['SERVER_PROTOCOL'] .'HTTP/1.1 401 Unauthorized', true, 401);
                            die;
                            break;
                    }
                    //Cantidad Totalizada
                    $neoTotalData=$neoMainQueryCounter->RecordCount();
                    $neoTotalFiltered=$neoTotalData;
                    //Generacion del JSON
                    $json_data =
                    array
                    (
                        "draw"=>intval($neoRequestData['draw']),
                        "recordsTotal"=>intval($neoTotalData),
                        "recordsFiltered"=>intval($neoTotalFiltered),
                        "data"=> $neoData
                    );
                    echo json_encode($json_data);
                    //Fin
                    break;
                default:
                    //Redirigir
                    header($_SERVER['SERVER_PROTOCOL'] .'HTTP/1.1 401 Unauthorized', true, 401);
                    die;
                    break;    
            }
            //Fin
            break;
        case 'GET':
            //Preparando Array
            $neoData = array();
            switch($neoTypeRequest)
            {
                case "neoMonthFinder":
                    //Para ajax de Select 2
                    $neoMainQuery=$neoConnectDB->Execute("SELECT codigotiquete,numerotiquete,asunto FROM calendario WHERE fechainicio LIKE '".$_GET["neoMonthRequired"]."%' AND (asunto LIKE '%".$_GET["neoTerm"]."%' OR numerotiquete LIKE '%".$_GET["neoTerm"]."%') AND codigoempresa='".$_SESSION["neoUserEnterpriseCode"]."' ");
                    while($neoMainData=$neoMainQuery->FetchRow())
                    {
                        $neoMainTicketCode=base64_encode($neoMainData["codigotiquete"]);
                        $neoMainTicketSubject=$neoMainData["asunto"];
                        $neoMainTicketNumber=$neoMainData["numerotiquete"];
                        $neoData =  array('id' => $neoMainTicketCode, 'text' => 'N° '.$neoMainTicketNumber.' - '.$neoMainTicketSubject);
                        //$neoData[] = $nestedData;
                    }
                    //Generacion del JSON
                    $json_data =
                    array
                    (
                        "results" => array($neoData),
                        "more" => false
                    );
                    break;
                default:
                    //Redirigir
                    header($_SERVER['SERVER_PROTOCOL'] .'HTTP/1.1 401 Unauthorized', true, 401);
                    die;
                    break;
            }
            echo json_encode($json_data);
            //Fin
            break;
        default:
            //Redirigir
            header($_SERVER['SERVER_PROTOCOL'] .'HTTP/1.1 401 Unauthorized', true, 401);
	        die;
            break;
    }
?>