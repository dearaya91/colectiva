<?php
    /*
        nekoFooter.php
    */
?>
        <footer class="nekoMainFooter" id="footer1-b" data-rv-view="1316">
            <div class="container">
                <div class="media-container-row content text-white">
                    <div class="row">
                        <div class="col-md-12 col-lg-3">
                            <div class="media-wrap">
                                <img id="nekoLogo" src="assets/images/logo2.png" alt="ColectivAndo-Logo" class="nekoFooterLogo"/>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-3 mbr-fonts-style display-7">
                            <h5 class="pb-3">
                                Dirección
                            </h5>
                            <p class="mbr-text">
                                San José, Costa Rica
                                <br>Avenida Mauro Fernández
                            </p>
                        </div>
                        <div class="col-md-12 col-lg-3 mbr-fonts-style display-7">
                            <h5 class="pb-3">
                                Contacto
                            </h5>
                            <p class="mbr-text">
                                Email: colectivaando@gmail.com
                                <br>Teléfono: +506 8917 8645
                            </p>
                        </div>
                        <div class="col-md-12 col-lg-3 mbr-fonts-style display-7">
                            <h5 class="pb-3">
                                Vinculos
                            </h5>
                            <p class="mbr-text">
                                <a class="text-white" href="login.php">Ingresar</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="footer-lower">
                    <div class="media-container-row">
                        <div class="col-lg-12">
                            <hr>
                        </div>
                    </div>
                    <div class="media-container-row mbr-white">
                        <div class="col-lg-6 copyright">
                            <p class="mbr-text mbr-fonts-style display-7">
                                ColectivAndo © Copyright <?php echo $nekoYearObtained; ?><br>Todos los Derechos Reservados
                            </p>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
            </div>
        </footer>