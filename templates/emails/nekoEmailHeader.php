<?php
    /*
        nekoEmailHeader.php
    */
    //HTML
    $nekoEmailHeaderHtml='
    <table class="spacer float-center">
        <tbody>
        <tr>
            <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td>
        </tr>
        </tbody>
    </table>
    <table align="center" class="container header float-center">
        <tbody>
        <tr>
            <td>
            <table class="row">
                <tbody>
                <tr>
                    <th class="small-12 large-12 columns first last">
                    <table>
                        <tr>
                        <th>
                            <h1 class="text-center">Correo de <strong>Omega Ticketer</strong></h1>
                            <center data-parsed="">
                                <table align="center" class="menu text-center float-center">
                                    <tr>
                                    <td>
                                        <table>
                                        <tr>
                                            <th class="menu-item float-center"><a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">Ingresar</a></th>
                                            <th class="menu-item float-center"><a href="#">Ayuda</a></th>
                                        </tr>
                                        </table>
                                    </td>
                                    </tr>
                                </table>
                            </center>
                        </th>
                        <th class="expander"></th>
                        </tr>
                    </table>
                    </th>
                </tr>
                </tbody>
            </table>
            </td>
        </tr>
        </tbody>
    </table>';
    //No HTML
    $nekoEmailHeaderNoHtml='Correo de Omega Ticketer';
?>