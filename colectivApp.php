<?php
    /*
        colectivApp.php
    */
    include("./controllers/nekoSessionControl.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            //Incluir Archivo
            include("./templates/nekoHeader.php");
        ?>
    </head>
    <body>
        <?php
            //Incluir Menu
            include("./templates/nekoMenu.php");
        ?>
        <!-- BANNER -->
        <section class="nekoMainBanner mbr-fullscreen mbr-parallax-background" id="header2-0" data-rv-view="1301" style="padding-bottom:0;min-height:0;">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
            </div>
            <div class="container align-center">
                <?php
                    //Incluir Archivo
                    include("./templates/nekoMessages.php");
                ?>
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-lg-10">
                        <h3 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-2">¡HOLA! <?php echo $_SESSION["nekoUserName"]; ?>, ESTE ES TU RECORRIDO REALIZADO &nbsp;<i class="fas fa-hands"></i></h3>
                        <hr>
                        <div class="full-width">
                            <div id="nekoMiMapa" style="width:100%;height:500px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- BANNER -->
        <!-- SUBSECTION -->
        <section class="header3 nekoSubsection01" id="header3-1" data-rv-view="1304">
            <div class="container">
                <div class="media-container-row">
                    <div class="media-content">
                        <h2 class="animate__animated animate__fadeInUp mbr-section-title mbr-white pb-3 mbr-fonts-style display-1">
                           Estadísticas
                        </h2>
                        <div class="mbr-section-text mbr-white pb-3 ">
                            <p class="mbr-text mbr-fonts-style display-4">
                                Observa tus Estadísticas Actuales. 
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>MIS RECORRIDOS REALIZADOS</h3>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-stripped" id="nekoMiTabla01">
                                        <thead>
                                            <tr>
                                                <th>N° Ruta</th>
                                                <th>Dirección</th>
                                                <th>Fecha</th>
                                                <th>Hora</th>
                                                <th>Guardiana</th>
                                                <th>Finalizado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>3554MP33</td>
                                                <td>Paso Ancho, San José</td>
                                                <td>02/23/2021</td>
                                                <td>03:55 p.m</td>
                                                <td>Fernanda</td>
                                                <td>Si</td>
                                            </tr>
                                            <tr>
                                                <td>3673MJ43</td>
                                                <td>Turrialba, Cartago</td>
                                                <td>03/01/2021</td>
                                                <td>07:56 a.m</td>
                                                <td>María</td>
                                                <td>Si</td>
                                            </tr>
                                            <tr>
                                                <td>1134JN77</td>
                                                <td>Avenida Central, San José</td>
                                                <td>03/14/2021</td>
                                                <td>05:55 a.m</td>
                                                <td>Mariela</td>
                                                <td>No</td>
                                            </tr>
                                            <tr>
                                                <td>4566HY76</td>
                                                <td>Santo Domingo, Heredia</td>
                                                <td>04/08/2021</td>
                                                <td>11:38 p.m</td>
                                                <td>Lucía</td>
                                                <td>Si</td>
                                            </tr>
                                            <tr>
                                                <td>2257MU55</td>
                                                <td>Liberia, Guanacaste</td>
                                                <td>01/27/2021</td>
                                                <td>01:34 p.m </td>
                                                <td>Alejamdra</td>
                                                <td>Si</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>N° Ruta</th>
                                                <th>Dirección</th>
                                                <th>Fecha</th>
                                                <th>Hora</th>
                                                <th>Guardiana</th>
                                                <th>Finalizado</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                                <div class="col-md-12">
                                    <hr>
                                    <h3>ACTIVIDADES EN LAS QUE PARTCIPE</h3>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-stripped" id="nekoMiTabla02">
                                            <thead>
                                                <tr>
                                                    <th>Foro</th>
                                                    <th>Tema</th>
                                                    <th>Fecha</th>
                                                    <th>Hora</th>
                                                    <th>Comentario</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>FORO0001</td>
                                                    <td>Género</td>
                                                    <td>3/12/2021</td>
                                                    <td>02:23:00 PM</td>
                                                    <td>¿Qué es la división sexual del trabajo?</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0022</td>
                                                    <td>Estudios femjnistas</td>
                                                    <td>03/08/2021</td>
                                                    <td>03:34 p.m</td>
                                                    <td>Sería interesante armar un grupo de lectura sobre el "Segundo sexo" de Beavouir"</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0022</td>
                                                    <td>Literatura</td>
                                                    <td>01/31/2021</td>
                                                    <td>7:45p.m</td>
                                                    <td>Recomiendo la lectura "Una habitación propia" de V. Wolf</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0003</td>
                                                    <td>Colectivas</td>
                                                    <td>02/20/2021</td>
                                                    <td>5:34 a.m</td>
                                                    <td>Vivo en Desamparados y me gustaria saber si cerca de aquí hay grupos de mujeres organizadas. Gracias</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0013</td>
                                                    <td>Hablémonos</td>
                                                    <td>02/04/2021</td>
                                                    <td>6:30 a.m </td>
                                                    <td>Necesitamos más circulos de mujeres en las zonas rurales</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Foro</th>
                                                    <th>Tema</th>
                                                    <th>Fecha</th>
                                                    <th>Hora</th>
                                                    <th>Comentario</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <hr>
                                    <h3>MIS INTERACCIONES EN FOROS</h3>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-stripped" id="nekoMiTabla03">
                                            <thead>
                                                <tr>
                                                    <th>N° Foro</th>
                                                    <th>Tema</th>
                                                    <th>Fecha</th>
                                                    <th>Hora</th>
                                                    <th>Participante</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>FORO0002</td>
                                                    <td>Género</td>
                                                    <td>05/01/2021</td>
                                                    <td>02:26:00 PM</td>
                                                    <td>Elena</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0030</td>
                                                    <td>Estudios feministas</td>
                                                    <td>02/05/2021</td>
                                                    <td>03:33 p.m</td>
                                                    <td>Karla</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0022</td>
                                                    <td>Literatura</td>
                                                    <td>03/14/2021</td>
                                                    <td>7:41p.m</td>
                                                    <td>Michelle</td>
                                                </tr>
                                                <tr>
                                                    <td>FOROR0012</td>
                                                    <td>Colectivas</td>
                                                    <td>03/05/2021</td>
                                                    <td>6:34 a.m</td>
                                                    <td>Marta</td>
                                                </tr>
                                                <tr>
                                                    <td>FORO0023</td>
                                                    <td>Hablémonos</td>
                                                    <td>01/15/2021</td>
                                                    <td>12:30 a.m </td>
                                                    <td>Fabiana</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>N° Foro</th>
                                                    <th>Tema</th>
                                                    <th>Fecha</th>
                                                    <th>Hora</th>
                                                    <th>Participante</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- SUBSECTION -->
        <!-- FOOTER -->
        <?php
            include("./templates/nekoFooter.php");
        ?>
        <!-- FOOTER -->
        <?php
            include("./templates/nekoScripts.php")
        ?>
        <script>
			$(document).ready(function()
			{
                var nekoUserIcon = L.icon(
                {
                    iconUrl: 'assets/images/avatars/3.1.png',
                    iconSize:     [45, 45], // size of the icon
                    iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                var nekoSafeIcon = L.icon(
                {
                    iconUrl: 'assets/images/avatars/safe.png',
                    iconSize:     [45, 45], // size of the icon
                    iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                var nekoDangerIcon = L.icon(
                {
                    iconUrl: 'assets/images/avatars/danger.png',
                    iconSize:     [45, 45], // size of the icon
                    iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                var arrMarkers=new Array(0);
                var mymap = L.map('nekoMiMapa').setView([9.938919,-84.0977827], 12);
				L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
				{
					maxZoom: 20,
					attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> Contribuyentes, ' +
						'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
						'Reservados todos los Derechos',
					id: 'mapbox/streets-v11',
					tileSize: 512,
					zoomOffset: -1
				}).addTo(mymap);
				L.marker([9.938919,-84.0977827], {icon: nekoUserIcon}).addTo(mymap).bindPopup("<strong>Actualmente estoy en:</strong>  San José, Costa Rica, Avenida Mauro Fernández").openPopup();
                
                function placeMarker(location,text) 
                {	
                    var marker = L.marker(location, {icon: nekoUserIcon},{title:text});
                    return marker;
                }

                function placeErrorMarker(location,text) 
                {	
                    var marker = L.marker(location, {icon: nekoDangerIcon},{title:text});
                    return marker;
                }

                function placeSafeMarker(location,text) 
                {	
                    var marker = L.marker(location, {icon: nekoSafeIcon},{title:text});
                    return marker;
                }

                function plotRandom(number)
                {
                    bounds = mymap.getBounds();
                    var southWest = bounds.getSouthWest();
                    var northEast = bounds.getNorthEast();
                    var lngSpan = northEast.lng - southWest.lng;
                    var latSpan = northEast.lat - southWest.lat;
                    pointsrand=[];
                    
                    for(var i=0;i<number;++i)
                    {
                        var point = [southWest.lat + latSpan * Math.random(),southWest.lng + lngSpan * Math.random()];
                        pointsrand.push(point);
                    }
                    
                    for(var i=0;i<number;++i)
                    {
                        var str_text=i+" : "+pointsrand[i];
                        var marker=placeMarker(pointsrand[i],str_text);
                        marker.addTo(mymap);
                        arrMarkers.push(marker);
                    }
                }

                function plotDangerRandom(number)
                {
                    bounds = mymap.getBounds();
                    var southWest = bounds.getSouthWest();
                    var northEast = bounds.getNorthEast();
                    var lngSpan = northEast.lng - southWest.lng;
                    var latSpan = northEast.lat - southWest.lat;
                    pointsrand=[];
                    
                    for(var i=0;i<number;++i)
                    {
                        var point = [southWest.lat + latSpan * Math.random(),southWest.lng + lngSpan * Math.random()];
                        pointsrand.push(point);
                    }
                    
                    for(var i=0;i<number;++i)
                    {
                        var str_text=i+" : "+pointsrand[i];
                        var marker=placeErrorMarker(pointsrand[i],str_text);
                        marker.addTo(mymap);
                        arrMarkers.push(marker);
                    }
                }

                function plotSafeRandom(number)
                {
                    bounds = mymap.getBounds();
                    var southWest = bounds.getSouthWest();
                    var northEast = bounds.getNorthEast();
                    var lngSpan = northEast.lng - southWest.lng;
                    var latSpan = northEast.lat - southWest.lat;
                    pointsrand=[];
                    
                    for(var i=0;i<number;++i)
                    {
                        var point = [southWest.lat + latSpan * Math.random(),southWest.lng + lngSpan * Math.random()];
                        pointsrand.push(point);
                    }
                    
                    for(var i=0;i<number;++i)
                    {
                        var str_text=i+" : "+pointsrand[i];
                        var marker=placeSafeMarker(pointsrand[i],str_text);
                        marker.addTo(mymap);
                        arrMarkers.push(marker);
                    }
                }
                //Hacerlo Random
                plotRandom(35);
                plotDangerRandom(40);
                plotSafeRandom(10);
            });
            
            $('#nekoMiTabla01').DataTable(
            {
                //Procesamiento
                "responsive": true,
                "pagingType": "full_numbers",
                "language":
                {
                    "searchPlaceholder" : "REGISTRO",
                    "lengthMenu": "Mostrando _MENU_ registos por página",
                    "zeroRecords": "No hay registos para mostrar por el momento",
                    "info": "Mostrando página _PAGE_ de _PAGES_, registos  _START_ al _END_ de  _TOTAL_ registos totales",
                    "infoEmpty": "No hay registos que mostrar",
                    "infoFiltered": "(filtrados de _MAX_ total registos)",
                    "processing": "Cargando más registos",
                    "search": "Buscar:",
                    "paginate":
                    {
                        "first":"|&#9668;",
                        "previous":"&#9668;",
                        "next":"&#9658;",
                        "last":"&#9658;|"
                    },
                    "oAria":
                    {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "lengthMenu": [[5,10,15,20], [5,10,15,20]],
                "stateSave": true,
                "processing": true,
            });

            $('#nekoMiTabla02').DataTable(
            {
                //Procesamiento
                "responsive": true,
                "pagingType": "full_numbers",
                "language":
                {
                    "searchPlaceholder" : "REGISTRO",
                    "lengthMenu": "Mostrando _MENU_ registos por página",
                    "zeroRecords": "No hay registos para mostrar por el momento",
                    "info": "Mostrando página _PAGE_ de _PAGES_, registos  _START_ al _END_ de  _TOTAL_ registos totales",
                    "infoEmpty": "No hay registos que mostrar",
                    "infoFiltered": "(filtrados de _MAX_ total registos)",
                    "processing": "Cargando más registos",
                    "search": "Buscar:",
                    "paginate":
                    {
                        "first":"|&#9668;",
                        "previous":"&#9668;",
                        "next":"&#9658;",
                        "last":"&#9658;|"
                    },
                    "oAria":
                    {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "lengthMenu": [[5,10,15,20], [5,10,15,20]],
                "stateSave": true,
                "processing": true,
            });

            $('#nekoMiTabla03').DataTable(
            {
                //Procesamiento
                "responsive": true,
                "pagingType": "full_numbers",
                "language":
                {
                    "searchPlaceholder" : "REGISTRO",
                    "lengthMenu": "Mostrando _MENU_ registos por página",
                    "zeroRecords": "No hay registos para mostrar por el momento",
                    "info": "Mostrando página _PAGE_ de _PAGES_, registos  _START_ al _END_ de  _TOTAL_ registos totales",
                    "infoEmpty": "No hay registos que mostrar",
                    "infoFiltered": "(filtrados de _MAX_ total registos)",
                    "processing": "Cargando más registos",
                    "search": "Buscar:",
                    "paginate":
                    {
                        "first":"|&#9668;",
                        "previous":"&#9668;",
                        "next":"&#9658;",
                        "last":"&#9658;|"
                    },
                    "oAria":
                    {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "lengthMenu": [[5,10,15,20], [5,10,15,20]],
                "stateSave": true,
                "processing": true,
            });
		</script>
    </body>
</html>