-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 22-03-2021 a las 01:47:40
-- Versión del servidor: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colectiva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE `archivos` (
  `codigoarchivo` bigint(20) NOT NULL,
  `nombrearchivo` varchar(512) NOT NULL,
  `tabla` varchar(32) NOT NULL,
  `ipremota` varchar(32) NOT NULL,
  `extension` varchar(8) NOT NULL,
  `peso` varchar(64) NOT NULL,
  `fechasubido` datetime NOT NULL,
  `autor` varchar(64) NOT NULL,
  `editor` varchar(64) NOT NULL,
  `ruta` text NOT NULL,
  `codigoEstado` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Detalles de los Archivos del APP';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadistica`
--

CREATE TABLE `estadistica` (
  `codigoestadistica` int(11) NOT NULL,
  `metodoprotocolo` varchar(16) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `accion` varchar(64) NOT NULL,
  `tabla` varchar(128) NOT NULL,
  `registro` varchar(64) NOT NULL,
  `pagina` text NOT NULL,
  `usuario` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Detalles Estadisticos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `codigoModulo` int(11) NOT NULL,
  `modulo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `paginas` text COLLATE utf8_unicode_ci NOT NULL,
  `roles` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Modulos Definidos para validar que la persona no entre';

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`codigoModulo`, `modulo`, `paginas`, `roles`) VALUES
(1, 'USUARIA', 'colectivApp,ColectivizAndo,TransitAndo,ConversAndo', '2'),
(2, 'ADMINISTRACIÓN', 'administrApp,ColectivizAndo,TransitAndo,ConversAndo', '1'),
(3, 'GUARDIANA', 'colectivApp,ColectivizAndo,TransitAndo,ConversAndo', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginasConfiguracion`
--

CREATE TABLE `paginasConfiguracion` (
  `codigoPagina` int(11) NOT NULL,
  `direccionPagina` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `tituloPagina` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Paginas para todos los usuarios';

--
-- Volcado de datos para la tabla `paginasConfiguracion`
--

INSERT INTO `paginasConfiguracion` (`codigoPagina`, `direccionPagina`, `tituloPagina`) VALUES
(1, 'preferences', 'Preferencias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `codigorol` int(11) NOT NULL,
  `rol` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`codigorol`, `rol`) VALUES
(1, 'ADMINISTRADORA'),
(2, 'USUARIA'),
(3, 'GUARDIANA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `codigousuario` bigint(20) NOT NULL,
  `nombreusuario` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contraseña` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `codigorol` int(11) NOT NULL,
  `estado` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Usuarios';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codigousuario`, `nombreusuario`, `correo`, `contraseña`, `codigorol`, `estado`) VALUES
(1, 'usuaria1', 'colectivaando@gmail.com', 'dXN1YXJpYTIwMjE=', 2, 'ACTIVO'),
(2, 'dearaya91', 'dearaya91@gmail.com', 'cmVzcGljZXBvbGx1bTk3', 1, 'ACTIVO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`codigoarchivo`),
  ADD KEY `codigoEstado` (`codigoEstado`);

--
-- Indices de la tabla `estadistica`
--
ALTER TABLE `estadistica`
  ADD PRIMARY KEY (`codigoestadistica`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`codigoModulo`);

--
-- Indices de la tabla `paginasConfiguracion`
--
ALTER TABLE `paginasConfiguracion`
  ADD PRIMARY KEY (`codigoPagina`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`codigorol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codigousuario`),
  ADD UNIQUE KEY `nombreusuario` (`nombreusuario`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD KEY `codigorol` (`codigorol`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`codigorol`) REFERENCES `roles` (`codigorol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
