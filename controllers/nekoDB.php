<?php
    /*
        nekoDB.php
    */
    //Inclusión de Abstacción de Conexión a Datos
    if( file_exists("vendor/adodb/adodb-php/adodb.inc.php") && is_readable("vendor/adodb/adodb-php/adodb.inc.php") && include("vendor/adodb/adodb-php/adodb.inc.php"))
    {
        include_once("vendor/adodb/adodb-php/adodb-errorhandler.inc.php");
        include_once("vendor/adodb/adodb-php/adodb.inc.php");
        include_once("vendor/adodb/adodb-php/adodb-pager.inc.php");
        include_once("vendor/adodb/adodb-php/tohtml.inc.php");
        include_once("vendor/adodb/adodb-php/adodb-active-record.inc.php");
        include_once("vendor/adodb/adodb-php/pivottable.inc.php");
    }
    else
    {
        include_once("../vendor/adodb/adodb-php/adodb-errorhandler.inc.php");
        include_once("../vendor/adodb/adodb-php/adodb.inc.php");
        include_once("../vendor/adodb/adodb-php/adodb-pager.inc.php");
        include_once("../vendor/adodb/adodb-php/tohtml.inc.php");
        include_once("../vendor/adodb/adodb-php/adodb-active-record.inc.php");
        include_once("../vendor/adodb/adodb-php/pivottable.inc.php");
    }
    //Control de errores
    error_reporting(E_ERROR);
    define('ADODB_ERROR_LOG_TYPE',3);
    define('ADODB_ERROR_LOG_DEST',__DIR__.'/errors.log');
    //Base de Datos Principal
    $nekoDriver="mysqli";
    $nekoConnectDB=ADONewConnection($nekoDriver);
    $nekoConnectDB->debug=false;
    $nekoServerNameDB="localhost";
    $nekoDBUserName="nekoUser";
    $nekoDBUserPassword="Nek@2021";
    $nekoDBDatabaseName="colectiva";
    $nekoConnectDB->Connect($nekoServerNameDB, $nekoDBUserName, $nekoDBUserPassword, $nekoDBDatabaseName);
    $nekoConnectDB->Execute("SET names 'utf8'"); //Set Chars
?>