<?php
    /*
        nekoMenu.php
    */
?>
        <section class="menu nekoHeaderMenu" once="menu" id="menu2-9" data-rv-view="1299">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-caption-wrap">
                            <img id="nekoLogo" src="assets/images/logo1.png" alt="ColectivAndo-Logo" class="nekoAppLogo"/>
                        </span>

                    <?php
                        if($_SESSION["nekoUserLogged"]==true)
                        {
                            echo '
                            <div class="navbar-buttons mbr-section-btn">
                                <a class="btn btn-sm btn-warning display-4" id="neoPanicAttack">
                                    Pánico <i class="fas fa-exclamation-triangle"></i>
                                </a>
                            </div>';
                        }
                    ?>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                        <?php
                            if($_SESSION["nekoUserLogged"]!=true)
                            {
                                echo
                                '
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link link text-white display-4">
                                        Inicio
                                    </a>
                                </li>';
                            }
                            else
                            {
                                echo
                                '
                                <li class="nav-item">
                                    <a href="'.$_SESSION["nekoUserMainPage"].'" class="nav-link link text-white display-4">
                                        Inicio
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="ColectivizAndo.php" class="nav-link link text-white display-4">
                                        ColectivizAndo
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="ConversAndo.php" class="nav-link link text-white display-4">
                                        ConversAndo
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="TransitAndo.php" class="nav-link link text-white display-4">
                                        TransitAndo
                                    </a>
                                </li>';
                            }
                        ?>
                    </ul>
                    <?php
                        if($_SESSION["nekoUserLogged"]!=true)
                        {
                            echo '
                            <div class="navbar-buttons mbr-section-btn">
                                <a class="btn btn-sm btn-primary display-4" href="login.php">
                                    IngresAndo
                                </a>
                            </div>';
                        }
                        else
                        {
                            echo '
                            <div class="navbar-buttons mbr-section-btn">
                                <a class="btn btn-sm btn-primary display-4" href="preferences.php">
                                    <i class="fas fa-cogs"></i>
                                </a>
                            </div>';
                            echo '
                            <div class="navbar-buttons mbr-section-btn">
                                <a class="btn btn-sm btn-primary display-4" href="index.php">
                                    Salir
                                </a>
                            </div>';
                        }
                    ?>
                </div>
            </nav>
        </section>