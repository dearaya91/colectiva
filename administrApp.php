<?php
    /*
        administrApp.php
    */
    include("./controllers/nekoSessionControl.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            //Incluir Archivo
            include("./templates/nekoHeader.php");
        ?>
        <script src='https://www.google.com/recaptcha/api.js?render=6LddHYkaAAAAAKj8GPsy-VTyzbroubDVKIjFdhQ3'></script>
    </head>
    <body>
        <?php
            //Incluir Menu
            include("./templates/nekoMenu.php");
        ?>
        <!-- BANNER -->
        <section class="nekoMainBanner mbr-fullscreen mbr-parallax-background" id="header2-0" data-rv-view="1301">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
            </div>
            <div class="container align-center">
                <?php
                    //Incluir Archivo
                    include("./templates/nekoMessages.php");
                ?>
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-lg-10">
                        <h3 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">INGRESA AL APLICATIVO</h3>
                        <img src="assets/images/logo1.png" alt="nekoLogoIngreso" style="width:300px;">
                        <hr>
                        <form id="nekoLogin" class="form-control container" method="POST" action="controllers/nekoStart.php">
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" id="nekoUserName" name="nekoUserName" class="form-control" placeholder="Nombre de Usuario o Correo" aria-label="Nombre de Usuario o Correo" data-rule-required="true" data-msg-required="SE REQUIERE NOMBRE DE USUARIO" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input type="password" id="nekoUserPassword" name="nekoUserPassword" class="form-control" placeholder="Contraseña" aria-label="Contraseña" data-rule-required="true" data-msg-required="SE REQUIERE LA CONSTRASEÑA" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mbr-section-btn float-left">
                                        <a class="btn btn-form btn-md btn-info display-3" href="index.php"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;  Volver</a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mbr-section-btn float-right">
                                        <button type="submit" id="nekoLoginBtn" class="btn btn-md btn-primary display-3"><i class="fas fa-check"></i>&nbsp;&nbsp; Ingresa Ya</button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="nekoToken" name="nekoToken" value="" readonly>
                            <input type="hidden" id="nekoAction" name="nekoAction" value="" readonly>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- BANNER -->
        <!-- CREDITS -->
        <!-- FOOTER -->
        <?php
            include("./templates/nekoFooter.php");
        ?>
        <!-- FOOTER -->
        <?php
            include("./templates/nekoScripts.php")
        ?>
        <script>
			$(document).ready(function()
			{
                // INGRESAR A CUENTA
                $("#nekoLogin").validate
                ({
                    onkeyup: false,
                    ignore:[],
                    doNotHideMessage: true,
                    errorElement: 'span',
                    errorClass: 'error-block',
                    focusInvalid: true,	
                    highlight: function(element) 
                    {
                        $(element).closest('.form-control').addClass('has-error');
                    },
                    unhighlight: function(element) 
                    {
                        $(element).closest('.form-control').removeClass('has-error');
                    },
                    errorPlacement: function(error, element) 
                    {
                        error.insertAfter(element.parent('.input-group'));
                    },
                    submitHandler: function(form)
                    {
                        //Bloquear Botón
                        $("#nekoLoginBtn").attr("disabled",true);
                        //Envío por Ajax
                        $.ajax
                        ({
                            type: "POST",
                            url: "./controllers/nekoStart.php",
                            data: $(form).serialize(),
                            success: function (data)
                            {
                                console.log(data);
                                //Ejecución Efectiva
                                swal("Excelente", "Usted ha creado su cuenta efectivamente, por favor proceda a ingresar", "success");
                                //Desbloquear Botón
                                $("#nekoLoginBtn").attr("disabled",false);
                                //Reiniciar Formulario
                                $(form)[0].reset();
                            },
                            error: function(data, errorThrown)
                            {
                                //No Procesamiento	
                                swal("Error", "Usted no ha podido crear su cuenta, por favor intentelo nuevamente", "error");
                                //Desbloquear Botón
                                $("#nekoLoginBtn").attr("disabled",false);
                            }
                        });
                        
                        //Google Recaptcha
                        grecaptcha.ready(function()
                        {
                            grecaptcha.execute('6LddHYkaAAAAAKj8GPsy-VTyzbroubDVKIjFdhQ3', {action: 'login'}).then(function(token)
                            {
                                // Verify the token on the server.
                                document.getElementById('nekoToken').value=token;
                                document.getElementById('nekoAction').value='login';
                            });
                        });
                        return false;
                    }
                });
                //Google Recaptcha
                grecaptcha.ready(function()
                {
                    grecaptcha.execute('6LddHYkaAAAAAKj8GPsy-VTyzbroubDVKIjFdhQ3', {action: 'login'}).then(function(token)
                    {
                        // Verify the token on the server.
                        document.getElementById('nekoToken').value=token;
                        document.getElementById('nekoAction').value='login';
                    });
			    });
            });
		</script>
    </body>
</html>