<?php
    /*
        TransitAndo.php
    */
    include("./controllers/nekoSessionControl.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            //Incluir Archivo
            include("./templates/nekoHeader.php");
        ?>
        <link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css" />
        <style>
            .leaflet-control-geocoder-form input
            {
                font-size: 120%;
                border: 0;
                background-color: transparent;
                width: 52vw;
            }
            .leaflet-control-geocoder-alternatives li
            {
                white-space: nowrap;
                display: block;
                overflow: hidden;
                padding: 5px 8px;
                text-overflow: ellipsis;
                border-bottom: 1px solid #ccc;
                cursor: pointer;
                color: #000;
                width: 52vw;
            }
        </style>
    </head>
    <body>
        <?php
            //Incluir Menu
            include("./templates/nekoMenu.php");
        ?>
        <!-- BANNER -->
        <section class="nekoMainBanner mbr-fullscreen mbr-parallax-background" id="header2-0" data-rv-view="1301" style="padding-bottom:0;min-height:0;">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
            </div>
            <div class="container align-center">
                <?php
                    //Incluir Archivo
                    include("./templates/nekoMessages.php");
                ?>
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-lg-10">
                        <h3 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">Iniciemos un Recorrido <i class="fas fa-map-marker"></i></h3>
                        <hr>
                        <div class="full-width">
                            <div id="nekoMiMapa" style="width:100%;height:500px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- BANNER -->
        <!-- SUBSECTION -->
        <section class="header3 nekoSubsection01" id="header3-1" data-rv-view="1304">
            <div class="container">
                <div class="media-container-row">
                    <div class="media-content">
                        <h2 class="animate__animated animate__fadeInUp mbr-section-title mbr-white pb-3 mbr-fonts-style display-2">
                           Detalles del Recorrido
                        </h2>
                        <div class="mbr-section-text mbr-white pb-3 ">
                            <p class="mbr-text mbr-fonts-style display-5">
                                Indicanos como quieres realizar el viaje. 
                            </p>
                            <form id="nekoTravelForm" method="POST">
                                <h3>Identifica los espacios de encuentro y seguridad</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>¿Viajar con Guardiana?</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <select id="nekoGuard" name="nekoGuard" class="form-control" data-rule-required="true" data-msg-required="SELECCIONE SI VA A VIAJAR CON GUARDIANA O NO">
                                                <option value="">SELECCIONE UNA OPCION DE GUARDIANA</option>
                                                <option value="no">NO QUIERO VIAJAR CON GUARDIANA</option>
                                                <option value="FATIMA">FATIMA</option>
                                                <option value="ERICA">ERICA</option>
                                                <option value="BEATRIZ">BEATRIZ</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Fecha de Viaje</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                            </div>
                                            <input id="nekoDate" name="nekoDate" type="text" class="form-control" placeholder="AAAA-MM-DD" data-rule-required="true" data-msg-required="REQUERIDO" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>¿Informar a otras usuarias?</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <select id="nekoFollow" name="nekoFollow" class="form-control" data-rule-required="true" data-msg-required="REQUERIDO">
                                                <option value="SI">SI INFORMAR A OTRAS USUARIAS</option>
                                                <option value="NO">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Punto de Encuentro</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                                            </div>
                                            <input id="nekoDate" name="nekoDate" type="text" class="form-control" placeholder="Detalle el Punto de Encuentro" data-rule-required="true" data-msg-required="REQUERIDO"/>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mbr-section-btn">
                                            <button type="submit" id="nekoBeginTravelBtn" class="btn btn-block btn-primary display-3"><i class="fas fa-check"></i>&nbsp;&nbsp; Empezar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- SUBSECTION -->
        <!-- FOOTER -->
        <?php
            include("./templates/nekoFooter.php");
        ?>
        <!-- FOOTER -->
        <?php
            include("./templates/nekoScripts.php")
        ?>
        <script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>
        <script>
			$(document).ready(function()
			{
                $("#nekoBeginTravelBtn").on('click touch', function()
                {
                    swal("Lo Sentimos", "Usted se encuentra actualmente en una versión demostrativa de la aplicación :( más adelante le entregaremos mejores accesos <3.", "info");
                });
                // CREAR CUENTA
                $("#nekoTravelForm").validate
                ({
                    onkeyup: false,
                    ignore:[],
                    doNotHideMessage: true,
                    errorElement: 'span',
                    errorClass: 'error-block',
                    focusInvalid: true,	
                    highlight: function(element) 
                    {
                        $(element).closest('.form-control').addClass('has-error');
                    },
                    unhighlight: function(element) 
                    {
                        $(element).closest('.form-control').removeClass('has-error');
                    },
                    errorPlacement: function(error, element) 
                    {
                        error.insertAfter(element.parent('.input-group'));
                    },
                    submitHandler: function(form)
                    {
                        swal("Lo Sentimos", "Usted se encuentra actualmente en una versión demostrativa de la aplicación :( más adelante le entregaremos mejores accesos <3.", "info");
                    }
                });

                var arrMarkers=new Array(0);

                var nekoUserIcon = L.icon(
                {
                    iconUrl: 'assets/images/avatars/3.1.png',
                    iconSize:     [45, 45], // size of the icon
                    iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                var nekoSafeIcon = L.icon(
                {
                    iconUrl: 'assets/images/avatars/safe.png',
                    iconSize:     [45, 45], // size of the icon
                    iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                var nekoDangerIcon = L.icon(
                {
                    iconUrl: 'assets/images/avatars/danger.png',
                    iconSize:     [45, 45], // size of the icon
                    iconAnchor:   [45, 45], // point of the icon which will correspond to marker's location
                    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                function placeErrorMarker(location,text) 
                {	
                    var marker = L.marker(location, {icon: nekoDangerIcon},{title:text});
                    return marker;
                }

                function placeSafeMarker(location,text) 
                {	
                    var marker = L.marker(location, {icon: nekoSafeIcon},{title:text});
                    return marker;
                }

                var mymap = L.map('nekoMiMapa').setView([9.938919,-84.0977827], 12);
				L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
				{
					maxZoom: 20,
					attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> Contribuyentes, ' +
						'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
						'Reservados todos los Derechos',
					id: 'mapbox/streets-v11',
					tileSize: 512,
					zoomOffset: -1
				}).addTo(mymap);
				L.marker([9.938919,-84.0977827], {icon: nekoUserIcon,}).addTo(mymap).bindPopup("<strong>Actualmente estoy en:</strong>  San José, Costa Rica, Avenida Mauro Fernández").openPopup();
                L.Control.geocoder(
                {
                    collapsed: false,
                    placeholder: 'Selecciona tu lugar de destino aquí...',
                    icon: nekoUserIcon
                }
                ).on('markgeocode', function(e) {
                    var bbox = e.geocode.bbox;
                    var nekoPoint = e.geocode.center;
                    var poly = L.polyline([nekoPoint, [9.938919,-84.0977827] ]).addTo(mymap);
                    /*
                    var poly = L.polygon([
                    bbox.getSouthEast(),
                    bbox.getNorthEast(),
                    bbox.getNorthWest(),
                    bbox.getSouthWest()
                    ]).addTo(mymap);*/
                    mymap.fitBounds(poly.getBounds());
                }).addTo(mymap);

                function plotDangerRandom(number)
                {
                    bounds = mymap.getBounds();
                    var southWest = bounds.getSouthWest();
                    var northEast = bounds.getNorthEast();
                    var lngSpan = northEast.lng - southWest.lng;
                    var latSpan = northEast.lat - southWest.lat;
                    pointsrand=[];
                    
                    for(var i=0;i<number;++i)
                    {
                        var point = [southWest.lat + latSpan * Math.random(),southWest.lng + lngSpan * Math.random()];
                        pointsrand.push(point);
                    }
                    
                    for(var i=0;i<number;++i)
                    {
                        var str_text=i+" : "+pointsrand[i];
                        var marker=placeErrorMarker(pointsrand[i],str_text);
                        marker.addTo(mymap);
                        arrMarkers.push(marker);
                    }
                }

                function plotSafeRandom(number)
                {
                    bounds = mymap.getBounds();
                    var southWest = bounds.getSouthWest();
                    var northEast = bounds.getNorthEast();
                    var lngSpan = northEast.lng - southWest.lng;
                    var latSpan = northEast.lat - southWest.lat;
                    pointsrand=[];
                    
                    for(var i=0;i<number;++i)
                    {
                        var point = [southWest.lat + latSpan * Math.random(),southWest.lng + lngSpan * Math.random()];
                        pointsrand.push(point);
                    }
                    
                    for(var i=0;i<number;++i)
                    {
                        var str_text=i+" : "+pointsrand[i];
                        var marker=placeSafeMarker(pointsrand[i],str_text);
                        marker.addTo(mymap);
                        arrMarkers.push(marker);
                    }
                }
                plotDangerRandom(40);
                plotSafeRandom(10);
            });
		</script>
    </body>
</html>