<?php
    /*
        nekoStart.php
    */
    define("RECAPTCHA_V3_SECRET_KEY",'6LddHYkaAAAAADgOcSxD8O8u-2BJ4w2taDQPAv3c');
    //Funciones
    include_once("./nekoFunctions.php");
    //Recaptcha
    //Captcha
    //Envío de Correos
    $oittoken=$_POST['nekoToken'];
    $oitaction=$_POST['nekoAction'];
    //Llamada al POST request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => RECAPTCHA_V3_SECRET_KEY, 'response' => $oittoken)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    $arrResponse = json_decode($response, true);
    //Verifica la respuesta y que recaptcha sea válido
    if($arrResponse["success"]=='1' && $arrResponse["action"]==$oitaction && $arrResponse["score"]>=0.5)
    {
        //Verificar si el usuario existe
        $nekoVerifyUserExistence=$nekoConnectDB->Execute("SELECT * FROM usuarios WHERE (nombreusuario='".$_POST["nekoUserName"]."' OR correo='".$_POST["nekoUserName"]."') AND contraseña='".base64_encode($_POST["nekoUserPassword"])."' ");
        if($nekoVerifyUserExistence->RecordCount() > 0)
        {
            session_start();
            //Abrir variables de Sesión
            $_SESSION["nekoUserLogged"]=true;
            while($nekoVerifyUserData=$nekoVerifyUserExistence->FetchRow())
            {
                //Datos
                $_SESSION["nekoUserCode"]=$nekoVerifyUserData["codigousuario"];
                $_SESSION["nekoUserName"]=$nekoVerifyUserData["nombreusuario"];
                $_SESSION["nekoUserEmail"]=$nekoVerifyUserData["correo"];
                $_SESSION["nekoUserPassword"]=$nekoVerifyUserData["contraseña"];
                $_SESSION["nekoUserRoleCode"]=$nekoVerifyUserData["codigorol"];
                $_SESSION["nekoUserRoleName"]=$nekoVerifyUserData["rolEstablecido"];
                $_SESSION["nekoUserDetailCode"]=$nekoVerifyUserData["detalleusuario"];
                $_SESSION["nekoUserSecurityQuestionCode"]=$nekoVerifyUserData["preguntaseguridad"];
                $_SESSION["nekoUserSecurityAnswer"]=$nekoVerifyUserData["respuesta"];
                $_SESSION["nekoUserSecurityColor"]=$nekoVerifyUserData["colorSeguridad"];
                $_SESSION["nekoUserFirstName"]=$nekoVerifyUserData["nombres"];
                $_SESSION["nekoUserLastName"]=$nekoVerifyUserData["apellidos"];
                $_SESSION["nekoUserProfilePicCode"]=$nekoVerifyUserData["fotoperfil"];
                $_SESSION["nekoUserBornDate"]=$nekoVerifyUserData["fechanacimiento"];
                $_SESSION["nekoUserEnterpriseCode"]=$nekoVerifyUserData["empresa"];
            }
            //Páginas de Configuración
            $nekoSelectAllConfigPagesArray=array();
            //Seleccionar Paginas de Configuración
            $nekoSelectAllConfigPagesSql=$nekoConnectDB->Execute("SELECT direccionPagina FROM paginasConfiguracion ");
            //While
            while($nekoSelectAllConfigPagesData=$nekoSelectAllConfigPagesSql->FetchRow())
            {
                $nekoSelectAllConfigPagesArray[]=$nekoSelectAllConfigPagesData["direccionPagina"];
            }
            //Seleccionar Paginas Asignadas
            $nekoSelectAllPagesSql=$nekoConnectDB->Execute("SELECT paginas FROM modulos WHERE roles LIKE '%".$_SESSION["nekoUserRoleCode"]."%' ORDER BY codigomodulo ASC LIMIT 1");
            //While
            while($nekoSelectAllPagesData=$nekoSelectAllPagesSql->FetchRow())
            {
                $nekoSelectAllPagesByRole=$nekoSelectAllPagesData["paginas"];
            }
            //Pages
            $nekoAllPagesArray=explode(',',$nekoSelectAllPagesByRole);
            //Guardando el Array en la sesión
            $_SESSION["nekoAllConfigPagesArray"]=$nekoSelectAllConfigPagesArray;
            $_SESSION["nekoAllPagesArray"]=$nekoAllPagesArray;
            //Si es cliente
            if($_SESSION["nekoUserRoleCode"]=="1")
            {
                //Relocalizar
                header("Location: ../administrApp.php");
                //Página Asignada
                $_SESSION["nekoUserMainPage"]="administrApp.php";
            }
            else
            {
                //Relocalizar
                header("Location: ../colectivApp.php");
                //Página Asignada
                $_SESSION["nekoUserMainPage"]="colectivApp.php";
            }
        }
        else
        {
            //Relocalizar
            header("Location: ../login.php?err=".base64_encode("x03")); //Error
        }
    }
    else
    {
        //Relocalizar
        header("Location: ../login.php?err=".base64_encode("x02")); //No paso la seguridad
    }
?>