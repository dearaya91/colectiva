<?php
    /*
        nekoHeader.php
    */
?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="<?php echo $nekoPageTitle." ".$nekoVersion; ?>, ColectivAndo.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png">
    <link rel="manifest" href="assets/images/site.webmanifest">
    <link rel="mask-icon" href="assets/images/safari-pinned-tab.svg" color="#652cb3">
    <meta name="msapplication-TileColor" content="#652cb3">
    <meta name="theme-color" content="#ffffff">
    <title><?php echo $nekoPageTitle; ?></title>
    <link rel="stylesheet" href="assets/lib/tether/tether.min.css">
    <link rel="stylesheet" href="assets/lib/DataTables/datatables.min.css">
    <link href='assets/lib/fullcalendar/scheduler/main.css' rel='stylesheet' />
    <link rel="stylesheet" href="assets/lib/leaflet/leaflet.css">
    <link rel="stylesheet" href="assets/lib/bootstrap-switch/css/bootstrap-switch.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/theme/css/custom.css" type="text/css">
