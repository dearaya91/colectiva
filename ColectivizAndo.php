<?php
    /*
        ColectivizAndo.php
    */
    include("./controllers/nekoSessionControl.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            //Incluir Archivo
            include("./templates/nekoHeader.php");
        ?>
    </head>
    <body>
        <?php
            //Incluir Menu
            include("./templates/nekoMenu.php");
        ?>
        <!-- BANNER -->
        <section class="nekoMainBanner mbr-fullscreen mbr-parallax-background" id="header2-0" data-rv-view="1301" style="padding-bottom:0;min-height:0;">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
            </div>
            <div class="container align-center">
                <?php
                    //Incluir Archivo
                    include("./templates/nekoMessages.php");
                ?>
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-lg-10">
                        <h3 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">Colectivizando</h3>
                    </div>
                </div>
            </div>
        </section>
        <!-- BANNER -->
        <!-- SUBSECTION -->
        <section class="header3 nekoSubsection01" id="header3-1" data-rv-view="1304">
            <div class="container">
                <div class="media-container-row">
                    <div class="media-content">
                        <h2 class="animate__animated animate__fadeInUp mbr-section-title mbr-white pb-3 mbr-fonts-style display-2">
                           Eventos y Agenda Disponibles <?php echo $nekoDateCompleteDetail; ?>
                        </h2>
                        <div class="mbr-section-text mbr-white pb-3 ">
                            <p class="mbr-text mbr-fonts-style display-4">
                                Informate, participa y orientante, porque no cuesta nada <i class="fas fa-hands"></i>
                            </p>
                        </div>
                        <div class="col-lg-12 container form-control">
                            <div id='nekoCalendar' style="width: 100%;height:500px;"></div>
                        </div>
                        <div class="mbr-section-text mbr-white pb-3 ">
                            <p class="mbr-text mbr-fonts-style display-4">
                                Participa sin Problema Alguno
                            </p>
                            <hr>
                            <h2 class="animate__animated animate__fadeInUp mbr-section-title mbr-white pb-3 mbr-fonts-style display-2">
                            Información, Orientación y Noticias disponibles <?php echo $nekoDateCompleteDetail; ?>
                            </h2>
                            <hr>
                            <table id="nekoForum" class="table table-stripped table-hover animate_animated animate_fadeInUp">
                                <thead>
                                    <tr>
                                        <th>
                                            FOROS ACTIVOS
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <!-- ACORDION -->
                                            <div id="nekoMainAccordion01">
                                                <!-- FORO -->
                                                <div class="card border-primary">
                                                    <div class="card-header text-success" id="nekoHeadingForum01" class="btn-link" data-toggle="collapse" data-target="#nekoForum01" aria-expanded="true" aria-controls="nekoHeadingForum01">
                                                        <h4><strong>Se anuncio nueva Ley en Contra del Acoso Callejero<span class="version status float-right"><i class="fas fa-file-alt"></i></span></strong></h4>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                    <div id="nekoForum01" class="collapse show" aria-labelledby="nekoForum01" data-parent="#nekoMainAccordion01">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Noticias: <span class="version status" >NOTI1003</span></h4>
                                                            <hr>
                                                            <p class="card-text"><span class="float-left">Fecha de Inicio: 3 de Diciembre de 2020</span></p>
                                                            <br>
                                                            <p class="card-text text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.....</p>
                                                            <br>
                                                            <a class="btn btn-lg btn-info" href="notice.php">Ver Noticia</a>
                                                        </div>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                </div>
                                                <div class="clearfix"></div>
                                                <!-- FORO -->
                                            </div>
                                            <!-- ACORDION -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <!-- ACORDION -->
                                            <div id="nekoMainAccordion02">
                                                <!-- FORO -->
                                                <div class="card border-primary">
                                                    <div class="card-header text-success" id="nekoHeadingForum02" class="btn-link" data-toggle="collapse" data-target="#nekoForum02" aria-expanded="true" aria-controls="nekoHeadingForum02">
                                                        <h4><strong>Nuevos Proyectos establecidos con los derechos de la mujer ¿Realidad o pura trama? <span class="version status float-right"><i class="fas fa-file-alt"></i></span></strong></h4>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                    <div id="nekoForum02" class="collapse" aria-labelledby="nekoForum02" data-parent="#nekoMainAccordion01">
                                                        <div class="card-body">
                                                        <h4 class="card-title">Noticias: <span class="version status" >NOTI1002</span></h4>
                                                            <hr>
                                                            <p class="card-text"><span class="float-left">Fecha de Inicio: 12 de Noviembre de 2020</span></p>
                                                            <br>
                                                            <p class="card-text text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.....</p>
                                                            <br>
                                                            <a class="btn btn-lg btn-info" href="notice.php">Ver Noticia</a>
                                                        </div>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                </div>
                                                <div class="clearfix"></div>
                                                <!-- FORO -->
                                            </div>
                                            <!-- ACORDION -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <!-- ACORDION -->
                                            <div id="nekoMainAccordion03">
                                                <!-- FORO -->
                                                <div class="card border-primary">
                                                    <div class="card-header text-success" id="nekoHeadingForum03" class="btn-link" data-toggle="collapse" data-target="#nekoForum03" aria-expanded="true" aria-controls="nekoHeadingForum03">
                                                        <h4><strong>Nueva marcha anunciada para solicitar nuevos derechos nuestros<span class="version status float-right"><i class="fas fa-file-alt"></i></span></strong></h4>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                    <div id="nekoForum03" class="collapse" aria-labelledby="nekoForum03" data-parent="#nekoMainAccordion01">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Noticias: <span class="version status" >NOTI1001</span></h4>
                                                            <hr>
                                                            <p class="card-text"><span class="float-left">Fecha de Inicio: 20 de Octubre de 2020</span></p>
                                                            <br>
                                                            <p class="card-text text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.....</p>
                                                            <br>
                                                            <a class="btn btn-lg btn-info" href="notice.php">Ver Noticia</a>
                                                        </div>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                </div>
                                                <div class="clearfix"></div>
                                                <!-- FORO -->
                                            </div>
                                            <!-- ACORDION -->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- SUBSECTION -->
        <!-- FOOTER -->
        <?php
            include("./templates/nekoFooter.php");
        ?>
        <!-- FOOTER -->
        <?php
            include("./templates/nekoScripts.php")
        ?>
        <script>
            $(document).ready(function()
            {
                //Tiquetes de Discusión
                $('#nekoForum').DataTable(
                {
                    //Procesamiento
                    "responsive": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "language":
                    {
                        "searchPlaceholder" : "FORO",
                        "lengthMenu": "Mostrando _MENU_ foros por página",
                        "zeroRecords": "No hay foros para mostrar por el momento",
                        "info": "Mostrando página _PAGE_ de _PAGES_, foros  _START_ al _END_ de  _TOTAL_ foros totales",
                        "infoEmpty": "No hay foros que mostrar",
                        "infoFiltered": "(filtrados de _MAX_ total foros)",
                        "processing": "Cargando más foros",
                        "search": "Buscar:",
                        "paginate":
                        {
                            "first":"|&#9668;",
                            "previous":"&#9668;",
                            "next":"&#9658;",
                            "last":"&#9658;|"
                        },
                        "oAria":
                        {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "lengthMenu": [[15,20,25,30], [15,20,25,30]],
                    "stateSave": true,
                    "processing": true,
                    "drawCallback": function( settings )
                    {
                        $("#nekoForum thead").remove();
                    }
                });
            });

            if($('#nekoCalendar').length)
            {
                //Dependiendo si existe el Contenido Cargado
                document.addEventListener('DOMContentLoaded', function()
                {
                    var calendarEl = document.getElementById('nekoCalendar');
                    var calendar = new FullCalendar.Calendar(calendarEl,
                    {
                        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
                        locale: 'es',
                        headerToolbar: {
                            center: 'dayGridMonth,timeGridWeek,listWeek' // buttons for switching between views
                        },
                        views: {
                            timeGridWeek:
                            {
                                type: 'timeGrid',
                                duration: { days: 7 },
                                buttonText: 'Semana',
                                nowIndicator: true,
                            },
                        },
                        initialView: 'listWeek',
                        firstDay: 1,
                        themeSystem: 'bootstrap',
                        eventOrder: 'title',
                        /* EVENTOS */
                        events: [
                            // Eventos
                            <?php
                                echo "
                                {
                                    title: 'INFORMATIVA DE LA MAÑANA',
                                    start: '2021-03-23T08:00:00',
                                    end: '2021-03-23T12:00:00',
                                    backgroundColor: '#88cd50',
                                    borderColor: '#88cd50',
                                    textColor: '#fff',
                                },
                                {
                                    title: 'SEMINARIO GENERAL INFORMATIVO',
                                    start: '2021-03-26T08:00:00',
                                    end: '2021-03-28T12:00:00',
                                    backgroundColor: '#88cd50',
                                    borderColor: '#88cd50',
                                    textColor: '#fff',
                                },
                                {
                                    title: 'CONVERSATORIO',
                                    start: '2021-03-30T08:00:00',
                                    end: '2021-03-31T01:00:00',
                                    backgroundColor: '#88cd50',
                                    borderColor: '#88cd50',
                                    textColor: '#fff',
                                },
                                ";
                            ?>
                        ],
                    });
                    calendar.render();
                });
            }
        </script>
    </body>
</html>