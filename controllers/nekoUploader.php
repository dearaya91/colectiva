<?php
    /*
    ========================================
    DOCUMENT TITLE: ajaxInputUploader.php
    Made by Fenrir
    Content: Omega Ticketer
    ===========================================
    */
    $nekoTotalRecurrent = count($_FILES[$nekoFileInputName]['name']); 
    foreach($_FILES as $nekoFilesIndex => $nekoFile)
    {
        //Ciclo de Repetición para Descomponer el Asunto
        for ($i = 0; $i < $nekoTotalRecurrent; $i++)
        {
            //Datos de los Archivos
            $nekoFileName=$nekoFile['name'][$i];
            $nekoTmpName=$nekoFile['tmp_name'][$i];
            $nekoFileSize=$nekoFile['size'][$i];
            $nekoFileType=$nekoFile['type'][$i];
            $nekoFileError=$nekoFile['error'][$i];
            //Verificar si hay errores en los archivos
            if(!empty($nekoFileError['error'][$i]))
            {
                //Retorna error
                return false;
            }
            //Agrega y mueve archivos, solamente si estos existen
            if(!empty($nekoTmpName) && is_uploaded_file($nekoTmpName))
            {
                //Procesamiento del Archivo		
                $nekoUploadFile="../repository/".$nekoSubUploadFolder.$nekoFileName;
                move_uploaded_file($nekoTmpName,$nekoUploadFile);
                chmod($nekoUploadFile,0777);
                //Nombre Renombrado
                $nekoFileNameArray=explode(".", $nekoFileName);
                $nekoFileExtension=end($nekoFileNameArray);
                $nekoFileNewName='NEO-'.time().''.nekoRandomStringGenerator().'.'.end($nekoFileNameArray);
                rename($nekoUploadFile, "../repository/".$nekoSubUploadFolder.$nekoFileNewName);
                $nekoFileName=$nekoFileNewName;
                //Registro de Archivos
                $nekoRegisterArchive=$nekoConnectDB->GenID($getuniqueregisternumber='secuenciasArchivos',$startID=1);
                $nekoFileSql=$nekoConnectDB->Execute("INSERT INTO archivos VALUES ('".$nekoRegisterArchive."','".$nekoFileName."','".$nekoTable."','".$nekoRemoteIP."','".$nekoFileExtension."','".$nekoFileSize."','".$nekoCurrentDateTime."','".$_SESSION["nekoUserName"]."','".$_SESSION["nekoUserName"]."','/repository/$nekoSubUploadFolder','14') ");
                //Verificando la Tabla
                if($nekoTable=="tiquetesSoporte")
                {
                    //Añadir al Array
                   $nekoTicketFilesCodes[] = $nekoRegisterArchive;
                }
            }
        }
    }
?>