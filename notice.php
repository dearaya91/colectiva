<?php
    /*
        notice.php
    */
    include("./controllers/nekoSessionControl.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            //Incluir Archivo
            include("./templates/nekoHeader.php");
        ?>
    </head>
    <body>
        <?php
            //Incluir Menu
            include("./templates/nekoMenu.php");
        ?>
        <!-- BANNER -->
        <section class="nekoMainBanner mbr-fullscreen mbr-parallax-background" id="header2-0" data-rv-view="1301" style="padding-bottom:0;min-height:0;">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
            </div>
            <div class="container align-center">
                <?php
                    //Incluir Archivo
                    include("./templates/nekoMessages.php");
                ?>
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-lg-10">
                        <h3 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">Comentemos Juntas <i class="fas fa-smile"></i></h3>
                    </div>
                </div>
            </div>
        </section>
        <!-- BANNER -->
        <!-- SUBSECTION -->
        <section class="header3 nekoSubsection01" id="header3-1" data-rv-view="1304">
            <div class="container">
                <div class="media-container-row">
                    <div class="media-content">
                        <h2 class="animate__animated animate__fadeInUp mbr-section-title mbr-white pb-3 mbr-fonts-style display-1">
                           Foros Activos a la Fecha <?php echo $nekoDateCompleteDetail; ?>
                        </h2>
                        <div class="mbr-section-text mbr-white pb-3 ">
                            <p class="mbr-text mbr-fonts-style display-4">
                                Participa sin Problema Alguno
                            </p>
                            <table id="nekoForum" class="table table-stripped table-hover animate_animated animate_fadeInUp">
                                <thead>
                                    <tr>
                                        <th>
                                            FOROS ACTIVOS
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <!-- ACORDION -->
                                            <div id="nekoMainAccordion01">
                                                <!-- FORO -->
                                                <div class="card border-primary">
                                                    <div class="card-header text-success" id="nekoHeadingForum01" class="btn-link" data-toggle="collapse" data-target="#nekoForum01" aria-expanded="true" aria-controls="nekoForum01">
                                                        <h4><strong>Hola, quisiera realizar una denuncia <span class="version status float-right"><i class="fas fa-sad-cry"></i></span></strong></h4>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                    <div id="nekoForum01" class="collapse" aria-labelledby="nekoHeadingForum01" data-parent="#nekoMainAccordion01">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Foro N°: <span class="version status" >AUM12009</span></h4>
                                                            <hr>
                                                            <p class="card-text"><span class="float-left">Fecha de Inicio: 3 de Diciembre de 2020</span></p>
                                                            <br>
                                                            <p class="card-text text-success">Hola quisiera que pudieran tener en cuenta esta denuncia que quiero realizar, por que es respecto a como estan funcionando las vías....</p>
                                                            <br>
                                                            <a class="btn btn-lg btn-info" href="forum.php">Ver Foro</a>
                                                        </div>
                                                    </div>
                                                    <!-- CONTENIDO DE COLAPSO -->
                                                </div>
                                                <div class="clearfix"></div>
                                                <!-- FORO -->
                                            </div>
                                            <!-- ACORDION -->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- SUBSECTION -->
        <!-- FOOTER -->
        <?php
            include("./templates/nekoFooter.php");
        ?>
        <!-- FOOTER -->
        <?php
            include("./templates/nekoScripts.php")
        ?>
        <script>
            $(document).ready(function()
            {
                //Tiquetes de Discusión
                $('#nekoForum').DataTable(
                {
                    //Procesamiento
                    "responsive": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "language":
                    {
                        "searchPlaceholder" : "FORO",
                        "lengthMenu": "Mostrando _MENU_ foros por página",
                        "zeroRecords": "No hay foros para mostrar por el momento",
                        "info": "Mostrando página _PAGE_ de _PAGES_, foros  _START_ al _END_ de  _TOTAL_ foros totales",
                        "infoEmpty": "No hay foros que mostrar",
                        "infoFiltered": "(filtrados de _MAX_ total foros)",
                        "processing": "Cargando más foros",
                        "search": "Buscar:",
                        "paginate":
                        {
                            "first":"|&#9668;",
                            "previous":"&#9668;",
                            "next":"&#9658;",
                            "last":"&#9658;|"
                        },
                        "oAria":
                        {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "lengthMenu": [[15,20,25,30], [15,20,25,30]],
                    "stateSave": true,
                    "processing": true,
                    "drawCallback": function( settings )
                    {
                        $("#nekoForum thead").remove();
                    }
                });
            });
        </script>
    </body>
</html>