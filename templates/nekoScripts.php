<?php
    /*
        nekoScripts.php
    */
?>
        <script src="assets/lib/jquery/dist/jquery.min.js"></script>
        <script src="assets/lib/jqueryValidator/jquery.validate.min.js"></script>
        <script src="assets/lib/jqueryValidator/additional-methods.min.js"></script>
        <script src="assets/lib/popper/popper.min.js"></script>
        <script src="assets/lib/tether/tether.min.js"></script>
        <script src="assets/lib/bootstrap/bootstrap.bundle.min.js"></script>
        <script src="assets/lib/smooth-scroll/smooth-scroll.js"></script>
        <script src="assets/lib/touch-swipe/touch-swipe.min.js"></script>
        <script src="assets/lib/jarallax/jarallax.min.js"></script>
        <script src="assets/lib/social-likes/social-likes.js"></script>
        <script src="assets/dropdown/js/script.min.js"></script>
        <script src="assets/lib/DataTables/datatables.min.js"></script>
        <script src="assets/lib/leaflet/leaflet.js"></script>
        <script src="assets/lib/sweetalert/sweetalert.min.js"></script>
        <script src='assets/lib/fullcalendar/scheduler/main.js'></script>
        <script src='assets/lib/fullcalendar/locales/es.js'></script>
        <script src="assets/theme/js/script.js"></script>