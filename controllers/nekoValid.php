<?php
    /*
        nekoValid.php
    */
    //PHP Mailer Classes for Namespace
    use PHPMailer\PHPMailer\Exception;
    //Funciones
    $nekoDocumentControllerAvailable=true;
    include_once("nekoFunctions.php");
    //Load Composer's autoloader
    require('../vendor/autoload.php');
    //Iniciar Sesión
    session_start();
    //Iniciando Objeto de Valores de Compañia
    $nekoCompanyClass = "";
    //Variables
    $nekoProcess=$_POST["nekoProcess"];
    //Condición con Switch
    switch($nekoProcess)
    {
        case "NEKOCOM001":
            //Insertar en la Base de Datos
            $nekoTicketConversationId=$nekoConnectDB->GetOne("SELECT MAX(codigoconversaciontiquete) FROM conversacionTiquete ");
            if($nekoTicketConversationId==NULL OR $nekoTicketConversationId=="0")
            {
                $nekoTicketConversationIdNext=1;
            }
            else
            {
                $nekoTicketConversationIdNext=$nekoTicketConversationId+1;
            }
            //Registrar en la base de Datos
            $nekoRegisterConversation=$nekoConnectDB->Execute("INSERT INTO conversacionTiquete VALUES('$nekoTicketConversationIdNext','".base64_decode($_POST["nekoTicketValue"])."','".trim($_POST["nekoTicketAddDiscussComments"])."','".$_SESSION["nekoUserCode"]."','".$_POST["nekoCurrentDate"]."','".$_POST["nekoCurrentHour"]."') ");
            include('registerStatistics.php');
            //Notificar
            $nekoTicketNumber=$nekoConnectDB->GetOne("SELECT numerotiquete FROM tiquetesSoporte WHERE codigotiquete='".base64_decode($_POST["nekoTicketValue"])."' ");
            $nekoNotifyMessages=$nekoConnectDB->GetOne("SELECT codigoNotificacion FROM tiquetesSoporte WHERE codigotiquete='".base64_decode($_POST["nekoTicketValue"])."' ");
            //================  Contenido del Mail ================ 
            $nekoEmailMainContent='
            <h4>Usted ha registrado en la fecha '.$nekoDateCompleteDetail.' un comentario del Tiquete '.$nekoTicketNumber.' con el siguiente comentario:</h4>
            <p>
                <ul>
                    <li><strong>Mensaje: </strong> '.trim($_POST["nekoTicketAddDiscussComments"]).'</li>
                </ul>
                Proceda a <a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">ingresar</a> al Help Desk para iniciar el proceso de Atención General.
                <br>
                <small>Nota: Seguirá recibiendo nuevas notificaciones del mismo para que tenga seguimiento de este tiquete.</small>
            </p>';
            $nekoEmailMainContentNoHtml='
            Usted ha registrado en la fecha '.$nekoDateCompleteDetail.' un comentario del Tiquete '.$nekoTicketNumber.' con el siguiente comentario:
                Mensaje: '.trim($_POST["nekoTicketAddDiscussComments"]).'
            
            Nota: Seguirá recibiendo nuevas notificaciones del mismo para que tenga seguimiento de este tiquete.
            ';
            //================  Contenido del Mail ================ 
            if($nekoNotifyMessages=="on")
            {
                //================ Enviar Correo Electrónico ========================
                try
                {
                    //Llamar a Mailer
                    include("./nekoMailer.php");
                    //Variables
                    $nekoEmailSubject='Conversación del Tiquete - '.$nekoTicketNumber;
                    //Incluir Variables
                    include("../template/emails/emailStyleSheet.php");
                    include("../template/emails/mainEmailTemplate.php"); //Template a Usar
                    //Recipients
                    $nekoMail->setFrom("dearaya91@gmail.com", "ColectivAndo",);
                    $nekoMail->addAddress($_SESSION["nekoUserEmail"]);     //Add a recipient
                    $nekoMail->addReplyTo("dearaya91@gmail.com", "ColectivAndo",);
                    $nekoMail->addBCC('luismora1997@gmail.com'); //Carbon Copy
                    //$nekoMail->addBCC('bcc@example.com'); //Carbon Copy Hidden
                    $nekoMail->AddEmbeddedImage('../assets/img/basic/logo-colored.svg', 'LOGO-ANDALUSA');    //Image Embedded
                    //Contenido
                    $nekoMail->isHTML(true);                                  //Set email format to HTML
                    $nekoMail->Subject = $nekoEmailSubject;
                    $nekoMail->Body    = $nekoEmailStyleSheet.$nekoEmailContentHtml;
                    $nekoMail->AltBody = $nekoEmailContentNoHtml;
                    //Char Set
                    $nekoMail->CharSet = 'UTF-8';
                    $nekoMail->Encoding = 'base64';
                    //Mensaje Enviado
                    $nekoMail->send();
                    //echo 'Mensaje Enviado';
                    $nekoMail->SmtpClose();
                } catch (Exception $e)
                {
                    //echo "El Mensaje no se Pudo Enviar. Mailer Error: {$nekoMail->ErrorInfo}";
                }
                //================ Fin Correo Electrónico ========================
            }
            //Imprimir Comentario
            if($_SESSION["nekoUserRoleCode"]=="4")
            {
                $nekoCommentClass="user-comment";
            }
            else
            {
                $nekoCommentClass="staff-comment";
            }
            $nekoUserTicketRoleSql=$nekoConnectDB->Execute("SELECT colorrol,color,rol FROM roles WHERE codigorol='".$_SESSION["nekoUserRoleCode"]."' ");
            while($nekoUserTicketRoleData=$nekoUserTicketRoleSql->FetchRow())
            {
                //Data
                $nekoTicketRoleBG=$nekoUserTicketRoleData["colorrol"];
                $nekoTicketRoleColor=$nekoUserTicketRoleData["color"];
                $nekoTicketRole=$nekoUserTicketRoleData["rol"];
            }
            echo '
            <div class="comment '.$nekoCommentClass.'">
                <div class="comment-content">
                    <p>
                        '.trim($_POST["nekoTicketAddDiscussComments"]).'
                    </p>
                </div>
                <cite class="commentator">
                    <span class="auth-dp"><img src="assets/img/author-2.jpg" alt="asd"></span>
                    '.$_SESSION["nekoUserName"].'
                    <small>'.$nekoCurrentDate.' '.$nekoCurrentTime.'</small>
                    <span style="background-color:'.$nekoTicketRoleBG.';color:'.$nekoTicketRoleColor.';">'.$nekoTicketRole.'</span>
                </cite>
            </div>
            ';
            break;
        case "NEKOPROC-0003":
            //Subir Archivos
            //Carpeta de Subida
            $nekoSubUploadFolder="notices/";
            $nekoTable="noticias";
            $nekoFileInputName="nekoFileExplorer";
            //Registrar en la Base de Datos
            include("./nekoUploader.php");
            //Registrar en la Tabla de Noticias
            //Ultimo Número de la Secuencia
            $nekoSelectLastIdNumber=$nekoConnectDB->GetOne("SELECT MAX(codigonoticia) FROM noticias ");
            if($nekoSelectLastIdNumber==NULL OR $nekoSelectLastIdNumber=="0")
            {
                $nekoSelectNextIdNumber=1;
            }
            else
            {
                $nekoSelectNextIdNumber=$nekoSelectLastIdNumber+1;
            }
            $nekoNoticesType=$_POST["nekoNoticesType"];
            $nekoNoticesContent=trim($_POST["nekoNoticesContent"]);
            $nekoNoticePublishDate=$_POST["nekoNoticePublishDate"];
            $nekoNoticeTitle=nekoStringUpperCase($_POST["nekoNoticeTitle"]);
            $nekoNoticeUrlObtained=$_POST["nekoNoticeUrlObtained"];
            $nekoNoticeStatusSelection=$_POST["nekoNoticeStatusSelection"];
            $nekoNoticesEnterprisesNotificationArray=$_POST["nekoNoticesEnterprisesNotification"];
            $nekoNoticesEnterprisesNotification=implode(',',$nekoNoticesEnterprisesNotificationArray);
            //Escribir
            if(empty($nekoRegisterArchive))
            {
                $nekoRegisterArchive="0";
            }
            //Dependiendo del tipo de Proceso
            if($_POST["nekoProcessType"]=="CREATE")
            {
                $nekoFileSql=$nekoConnectDB->Execute("INSERT INTO noticias VALUES ('".$nekoSelectNextIdNumber."','$nekoNoticesType','$nekoNoticePublishDate','".$nekoNoticeTitle."','".$nekoNoticeUrlObtained."','$nekoNoticesContent','".$nekoRegisterArchive."','".$nekoNoticeStatusSelection."','".$nekoNoticesEnterprisesNotification."','".$_SESSION["nekoUserCode"]."','".$_SESSION["nekoUserName"]."','','') ");
                //================ Enviar Correo Electrónico ========================
                //================  Contenido del Mail ================ 
                $nekoEmailMainContent='
                <h4><strong>'.$nekoNoticeTitle.'</strong> <br> Publicado en la fecha: '.$nekoNoticePublishDate.'</h4>
                <p>
                    <ul>
                        <li><strong>Obtenido de: </strong> '.$nekoNoticeUrlObtained.'</li>
                        <li><strong>Redactor: </strong> '.$_SESSION["nekoUserName"].'</li>
                    </ul>
                    <hr>
                        '.$nekoNoticesContent.'
                    <hr>
                    Proceda a <a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">ingresar</a> al Help Desk para obtener más información de la noticia.
                    <br>
                    <small>Nota: Puede que este correo tenga anexos.</small>
                </p>';
                $nekoEmailMainContentNoHtml='
                '.$nekoNoticeTitle.' \n Publicado en la fecha: '.$nekoNoticePublishDate.' '.$nekoCurrentTime.'
                    Obtenido de: '.$nekoNoticeUrlObtained.'
                    Redactor: '.$_SESSION["nekoUserName"].'

                    '.$nekoNoticesContent.'

                Proceda Ingresar al Help Desk para obtener más información de la noticia.
                Nota: Puede que este correo tenga anexos.';
                //================  Contenido del Mail ================
            }
            else if($_POST["nekoProcessType"]=="UPDATE") //Actualización de Noticia
            {
                //Obtener Código
                $nekoNoticeCode=base64_decode($_POST["nekoNoticeCode"]);
                $nekoObtainPreviousEditors=$nekoConnectDB->GetOne("SELECT editor FROM noticias WHERE codigonoticia='$nekoNoticeCode' ");
                if(!empty($nekoObtainPreviousEditors))
                {
                    $nekoObtainPreviousEditorsArray=explode(',',$nekoObtainPreviousEditors);
                    if(!in_array($_SESSION["nekoUserName"], $nekoObtainPreviousEditorsArray))
                    {
                        $nekoActualEditors=$nekoObtainPreviousEditors.','.$_SESSION["nekoUserName"];
                    }
                    else
                    {
                        $nekoActualEditors=$nekoObtainPreviousEditors;
                    }
                }
                else
                {
                    $nekoActualEditors=$_SESSION["nekoUserName"];
                }
                if($nekoRegisterArchive!="" AND !empty($nekoRegisterArchive))
                {
                    //SQL
                    $nekoFileSql=$nekoConnectDB->Execute("UPDATE noticias SET codigoTipoNoticia='$nekoNoticesType',fechanoticia='$nekoNoticePublishDate',titulonoticia='".$nekoNoticeTitle."',fuentenoticia='".$nekoNoticeUrlObtained."',contenidonoticia='$nekoNoticesContent',codigoarchivo='".$nekoRegisterArchive."',codigoEstado='".$nekoNoticeStatusSelection."',empresasNotificar='".$nekoNoticesEnterprisesNotification."',codigousuario='".$_SESSION["nekoUserCode"]."',editor='".$nekoActualEditors."' WHERE codigonoticia='$nekoNoticeCode' ");
                }
                else
                {
                    //SQL
                    $nekoFileSql=$nekoConnectDB->Execute("UPDATE noticias SET codigoTipoNoticia='$nekoNoticesType',fechanoticia='$nekoNoticePublishDate',titulonoticia='".$nekoNoticeTitle."',fuentenoticia='".$nekoNoticeUrlObtained."',contenidonoticia='$nekoNoticesContent',codigoEstado='".$nekoNoticeStatusSelection."',empresasNotificar='".$nekoNoticesEnterprisesNotification."',codigousuario='".$_SESSION["nekoUserCode"]."',editor='".$nekoActualEditors."' WHERE codigonoticia='$nekoNoticeCode' ");
                }
                //================ Enviar Correo Electrónico ========================
                //================  Contenido del Mail ================ 
                $nekoEmailMainContent='
                <h4><strong>'.$nekoNoticeTitle.'</strong> <br> Actualizado en la fecha: '.$nekoNoticePublishDate.'</h4>
                <p>
                    <ul>
                        <li><strong>Obtenido de: </strong> '.$nekoNoticeUrlObtained.'</li>
                        <li><strong>Redactor: </strong> '.$_SESSION["nekoUserName"].'</li>
                    </ul>
                    <hr>
                        '.$nekoNoticesContent.'
                    <hr>
                    Proceda a <a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">ingresar</a> al Help Desk para obtener más información de la noticia.
                    <br>
                    <small>Nota: Puede que este correo tenga anexos.</small>
                </p>';
                $nekoEmailMainContentNoHtml='
                '.$nekoNoticeTitle.' \n Publicado en la fecha: '.$nekoNoticePublishDate.' '.$nekoCurrentTime.'
                    Obtenido de: '.$nekoNoticeUrlObtained.'
                    Redactor: '.$_SESSION["nekoUserName"].'

                    '.$nekoNoticesContent.'

                Proceda Ingresar al Help Desk para obtener más información de la noticia.
                Nota: Puede que este correo tenga anexos.';
                //================  Contenido del Mail ================
            }
            //Enviar Correos
            //Mail
            try
            {
                //Llamar a Mailer
                include("./nekoMailer.php");
                //Variables
                $nekoEmailSubject='Noticia de Omega Ticketer: '.$nekoNoticeTitle.'';
                //Incluir Variables
                include("../template/emails/emailStyleSheet.php");
                include("../template/emails/mainEmailTemplate.php"); //Template a Usar
                //Recipients
                $nekoMail->setFrom("dearaya91@gmail.com", "ColectivAndo",);
                $nekoMail->addAddress($_SESSION["nekoUserEmail"]); //Add a recipient
                $nekoMail->addReplyTo("dearaya91@gmail.com", "ColectivAndo",);
                //Correos en General del NewsLetter
                if($nekoNoticesEnterprisesNotification!="*")
                {
                    foreach($nekoNoticesEnterprisesNotificationArray AS $nekoEnterpriseNoticeDeliverCode)
                    {
                        //Comienzan Correos de la Empresa
                        $nekoSelectEnterpriseMainMail=$nekoConnectDB->GetOne("SELECT correoContacto FROM empresas WHERE codigoempresa='$nekoEnterpriseNoticeDeliverCode' ");
                        //Mail Principal
                        $nekoMail->addAddress($nekoSelectEnterpriseMainMail); //Add a recipient
                        $nekoSelectEnterpriseColaboratorsMailSql=$nekoConnectDB->Execute("SELECT correo FROM usuariosSesion WHERE empresa='$nekoEnterpriseNoticeDeliverCode' ");
                        while($nekoSelectEnterpriseColaboratorsMailData=$nekoSelectEnterpriseColaboratorsMailSql->FetchRow())
                        {
                            //Empresa
                            $nekoEnterpriseCollaborator=$nekoSelectEnterpriseColaboratorsMailData["correo"];
                            //Si hay, añadir los correos de los colaboradores
                            $nekoMail->addAddress($nekoEnterpriseCollaborator);     //Add a recipient
                        }
                    }
                }
                else
                {
                    $nekoNoticesEnterprisesNotificationNewArray=array();
                    $nekoSelectAllEnterprisesSql=$nekoConnectDB->Execute("SELECT codigoempresa FROM empresas ");
                    while($nekoSelectAllEnterprisesData=$nekoSelectAllEnterprisesSql->FetchRow())
                    {
                        $nekoNoticesEnterprisesNotificationNewArray[]=$nekoSelectAllEnterprisesData["codigoempresa"];
                    }
                    //Para Cada
                    foreach($nekoNoticesEnterprisesNotificationNewArray AS $nekoEnterpriseNoticeDeliverCode)
                    {
                        //Comienzan Correos de la Empresa
                        $nekoSelectEnterpriseMainMail=$nekoConnectDB->GetOne("SELECT correoContacto FROM empresas WHERE codigoempresa='$nekoEnterpriseNoticeDeliverCode' ");
                        //Mail Principal
                        $nekoMail->addAddress($nekoSelectEnterpriseMainMail); //Add a recipient
                        $nekoSelectEnterpriseColaboratorsMailSql=$nekoConnectDB->Execute("SELECT correo FROM usuariosSesion WHERE empresa='$nekoEnterpriseNoticeDeliverCode' ");
                        while($nekoSelectEnterpriseColaboratorsMailData=$nekoSelectEnterpriseColaboratorsMailSql->FetchRow())
                        {
                            //Empresa
                            $nekoEnterpriseCollaborator=$nekoSelectEnterpriseColaboratorsMailData["correo"];
                            //Si hay, añadir los correos de los colaboradores
                            $nekoMail->addAddress($nekoEnterpriseCollaborator);     //Add a recipient
                        }
                    }
                }
                //Fin de Correos de la Empresa
                $nekoMail->addBCC('luismora1997@gmail.com'); //Carbon Copy
                $nekoMail->AddEmbeddedImage('../assets/img/basic/logo-colored.svg', 'LOGO-ANDALUSA');    //Image Embedded
                //Attachments
                $nekoFileName = $nekoConnectDB->GetOne("SELECT nombrearchivo FROM archivos WHERE codigoarchivo='$nekoRegisterArchive' ");
                $nekoMail->addAttachment("../repository/".$nekoSubUploadFolder.$nekoFileName); //Add attachments
                //Contenido
                $nekoMail->isHTML(true); //Set email format to HTML
                $nekoMail->Subject = $nekoEmailSubject;
                $nekoMail->Body    = $nekoEmailStyleSheet.$nekoEmailContentHtml;
                $nekoMail->AltBody = $nekoEmailContentNoHtml;
                //Char Set
                $nekoMail->CharSet = 'UTF-8';
                $nekoMail->Encoding = 'base64';
                //Mensaje Enviado
                $nekoMail->send();
                //echo 'Mensaje Enviado';
                $nekoMail->SmtpClose();
            } catch (Exception $e)
            {
                echo "El Mensaje no se Pudo Enviar. Mailer Error: {$nekoMail->ErrorInfo}";
            }
            break;
         case "NEKOPROC-0002":
            //Obteniendo Datos
            $nekoTicketNumber=base64_decode($_POST["nekoTicketNumber"]);
            $nekoTicketCode=base64_decode($_POST["nekoTicketCode"]);
            $nekoTicketSupportType=$_POST["nekoTicketSupportType"];
            $nekoTicketSupportSubject=nekoStringUpperCase($_POST["nekoTicketSupportSubject"]);
            $nekoTicketPriority=$_POST["nekoTicketPriority"];
            $nekoTicketPriorityName=$nekoConnectDB->GetOne("SELECT nombreprioridad FROM codigosPrioridad WHERE codigoprioridad='$nekoTicketPriority' ");
            $nekoTicketBeginDate=$_POST["nekoTicketBeginDate"];
            $nekoTicketExpirationDate=$_POST["nekoTicketExpirationDate"];
            $nekoTicketEndDate=$_POST["nekoTicketEndDate"];
            if(empty($nekoTicketEndDate))
            {
                $nekoTicketEndDate="0000-00-00 00:00:00";
            }
            $nekoTicketStatus=$_POST["nekoTicketStatus"];
            $nekoTicketStatusName=$nekoConnectDB->GetOne("SELECT estado FROM estados WHERE codigoEstado='$nekoTicketStatus' ");
            $nekoTicketEnterpriseAssigned=$_POST["nekoTicketEnterpriseAssigned"];
            $nekoTicketInvolvedUsers=implode(',',$_POST["nekoTicketInvolvedUsers"]);
            //Seleccionar el usuario más antiguo de esa organización
            $nekoSelectUserCode=$nekoConnectDB->GetOne("SELECT codigousuario FROM detallesUsuario WHERE codigoempresa='$nekoTicketEnterpriseAssigned' ORDER BY codigousuario ASC "); //Despues ver quien seria el gerente
            $nekoChangeTaskHours=$_POST["nekoChangeTaskHours"];
            $nekoFollowUp=$_POST["nekoFollowUp"];
            if(empty($nekoFollowUp))
            {
                $nekoFollowUp="off";
            }
            $nekoSupportTaskComments=trim($_POST["nekoSupportTaskComments"]);
            //Creación de Tiquete
            if($_POST["nekoProcessType"]=="CREATE")
            {
                //Ultimo Número de la Secuencia
                $nekoSelectNextIdNumber=$nekoConnectDB->GenID($getuniqueregisternumber='secuenciasTiquetesSoporte',$startID=1);
                //Crear Codificación
                $nekoMainTicketCoder=$nekoCompanyClass::nekoUniqueCodification;
                if(strlen($nekoSelectNextIdNumber)=="1")
                {
                    $nekoTicketNumberGenerated=$nekoMainTicketCoder.''.$nekoYearObtained.'-000'.$nekoSelectNextIdNumber;
                }
                else if(strlen($nekoSelectNextIdNumber)=="2")
                {
                    $nekoTicketNumberGenerated=$nekoMainTicketCoder.''.$nekoYearObtained.'-00'.$nekoSelectNextIdNumber;
                }
                else if(strlen($nekoSelectNextIdNumber)=="3")
                {
                    $nekoTicketNumberGenerated=$nekoMainTicketCoder.''.$nekoYearObtained.'-0'.$nekoSelectNextIdNumber;
                }
                else
                {
                    $nekoTicketNumberGenerated=$nekoMainTicketCoder.''.$nekoYearObtained.'-'.$nekoSelectNextIdNumber;
                }
                $nekoTicketNumber=$nekoTicketNumberGenerated;
                //Carpeta de Subida
                $nekoSubUploadFolder="tickets/".$nekoTicketNumber."/";
                mkdir("../repository/".$nekoSubUploadFolder, 0777);
                $nekoTable="tiquetesSoporte";
                $nekoFileInputName="nekoFileExplorer";
                //Array
                $nekoTicketFilesCodes=array();
                //Registrar en la Base de Datos
                include("./nekoUploader.php");
                //Separar archivos con Comas
                $nekoTicketFiles=implode(',', $nekoTicketFilesCodes);
                //Obtener el Codigo de la Empresa, el estado es 12 que es Tiquete Temporal -> Recordar Colocar los Involucrados de Manera Dinamica
                $nekoTicketSupportSql=$nekoConnectDB->Execute("INSERT INTO tiquetesSoporte VALUES('$nekoSelectNextIdNumber','$nekoTicketNumber','$nekoSelectUserCode','$nekoTicketSupportType','$nekoTicketInvolvedUsers','$nekoTicketSupportSubject','$nekoTicketPriority','$nekoTicketBeginDate','$nekoChangeTaskHours','$nekoTicketEndDate','$nekoTicketExpirationDate','$nekoSupportTaskComments','','$nekoTicketStatus','0','$nekoFollowUp','$nekoTicketFiles') ");
                //================  Contenido del Mail ================
                //Creado por Primera Vez
                $nekoEmailMainContent='
                <h4>Se ha registrado exitosamente un tiquete en la fecha '.$nekoDateCompleteDetail.' para su compañia bajo los siguientes detalles:</h4>
                <p>
                    <ul>
                    <li><strong>Asunto: </strong> '.$nekoTicketSupportSubject.'</li>
                    <li><strong>Prioridad: </strong> '.$nekoTicketPriorityName.'</li>
                    <li><strong>Comentarios: </strong> '.$nekoSupportTaskComments.'</li>
                    <li><strong>Estado: </strong> '.$nekoTicketStatusName.'</li>
                    </ul>
                    Proceda a <a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">ingresar</a> al Help Desk para iniciar el proceso de Atención General.
                    <br>
                    <small>Nota: Cabe destacar que si usted envio anexos, estos estarán tambien en este correo.</small>
                </p>';
                $nekoEmailMainContentNoHtml='
                Usted se ha registrado exitosamente un tiquete en la fecha '.$nekoDateCompleteDetail.' bajo los siguientes detalles:
                    Asunto: '.$nekoTicketSupportSubject.'
                    Prioridad: </strong> '.$nekoTicketPriorityName.'
                    Comentarios: '.$nekoSupportTaskComments.'
                    Estado: '.$nekoTicketStatusName.'
                
                Proceda Ingresar al Help Desk para iniciar el proceso de Atención General.
                Nota: Cabe destacar que si usted envio anexos, estos estarán tambien en este correo.
                ';
                //================  Contenido del Mail ================
            }
            else if($_POST["nekoProcessType"]=="UPDATE") //Actualización de Tiquete
            {
                //Seleccionar todo el String
                $nekoPreviousFilesString=$nekoConnectDB->GetOne("SELECT codigoarchivos FROM tiquetesSoporte WHERE numerotiquete='$nekoTicketNumber'");
                //Carpeta de Subida
                $nekoSubUploadFolder="tickets/".$nekoTicketNumber."/";
                $nekoTable="tiquetesSoporte";
                $nekoFileInputName="nekoFileExplorer";
                //Array
                $nekoTicketFilesCodes=array();
                //Registrar en la Base de Datos
                include("./nekoUploader.php");
                //Separar archivos con Comas
                $nekoTicketFiles=implode(',', $nekoTicketFilesCodes); //<-- Despues ver como manejar el asunton de la actualización de Documentos
                //Añadir al String si existen previos
                if(!empty($nekoTicketFilesCodes))
                {
                    if(!empty($nekoPreviousFilesString))
                    {
                        $nekoActualStringOfFiles=$nekoPreviousFilesString.','.$nekoTicketFiles;
                    }
                    else
                    {
                        $nekoActualStringOfFiles=$nekoTicketFiles;
                    }
                }
                else
                {
                    $nekoActualStringOfFiles=$nekoPreviousFilesString;
                }
                //Actualizar
                $nekoTicketSupportSql=$nekoConnectDB->Execute("UPDATE tiquetesSoporte SET codigousuario='$nekoSelectUserCode',codigoTipoTiquete='$nekoTicketSupportType',codigoinvolucrados='$nekoTicketInvolvedUsers',asunto='$nekoTicketSupportSubject',codigoprioridad='$nekoTicketPriority',fechainicio='$nekoTicketBeginDate',horas='$nekoChangeTaskHours',fechafin='$nekoTicketEndDate',fechavencimiento='$nekoTicketExpirationDate',respuestaFinal='$nekoSupportTaskComments',codigoEstado='$nekoTicketStatus',codigoNotificacion='$nekoFollowUp',codigoarchivos='$nekoActualStringOfFiles' WHERE codigotiquete='$nekoTicketCode' ");
                //================  Contenido del Mail ================
                //Regenerated
                $nekoEmailMainContent='
                <h4>Se ha actualizado un tiquete en la fecha '.$nekoDateCompleteDetail.' bajo los siguientes detalles:</h4>
                <p>
                    <ul>
                        <li><strong>Asunto: </strong> '.$nekoTicketSupportSubject.'</li>
                        <li><strong>Prioridad: </strong> '.$nekoTicketPriorityName.'</li>
                        <li><strong>Respuesta: </strong> '.$nekoSupportTaskComments.'</li>
                        <li><strong>Estado: </strong> '.$nekoTicketStatusName.'</li>
                    </ul>
                    Proceda a <a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">ingresar</a> al Help Desk para iniciar el proceso de Atención General.
                    <br>
                    <small>Nota: Cabe destacar que si usted envio anexos, estos estarán tambien en este correo.</small>
                </p>';
                $nekoEmailMainContentNoHtml='
                Se ha actualizado un tiquete en la fecha '.$nekoDateCompleteDetail.' bajo los siguientes detalles:
                    Asunto: '.$nekoTicketSupportSubject.'
                    Prioridad: </strong> '.$nekoTicketPriorityName.'
                    Comentarios: '.$nekoSupportTaskComments.'
                    Estado: '.$nekoTicketStatusName.'
                
                Proceda Ingresar al Help Desk para iniciar el proceso de Atención General.
                Nota: Cabe destacar que si usted envio anexos, estos estarán tambien en este correo.
                ';
                //================  Contenido del Mail ================ 
            }
            //Crear Correo
            //Statistics
            include_once("./registerStatistics.php");
            //================ Enviar Correo Electrónico ========================
            try
            {
                //Llamar a Mailer
                include("./nekoMailer.php");
                //Variables
                $nekoEmailSubject='Asignación de Tiquete - '.$nekoTicketNumber;
                //Incluir Variables
                include("../template/emails/emailStyleSheet.php");
                include("../template/emails/mainEmailTemplate.php");
                //Recipients
                $nekoMail->setFrom("dearaya91@gmail.com", "ColectivAndo",);
                $nekoMail->addAddress($_SESSION["nekoUserEmail"]);     //Add a recipient
                $nekoMail->addReplyTo("dearaya91@gmail.com", "ColectivAndo",);
                $nekoMail->addBCC('luismora1997@gmail.com'); //Carbon Copy
                //Attachments
                foreach($nekoTicketFilesCodes AS $nekoFilesCode)
                {
                    $nekoFileName = $nekoConnectDB->GetOne("SELECT nombrearchivo FROM archivos WHERE codigoarchivo='$nekoFilesCode' ");
                    $nekoMail->addAttachment("../repository/".$nekoSubUploadFolder.$nekoFileName); //Add attachments
                }
                $nekoMail->AddEmbeddedImage('../assets/img/basic/logo-colored.svg', 'LOGO-ANDALUSA');    //Image Embedded
                //Contenido
                $nekoMail->isHTML(true);                                  //Set email format to HTML
                $nekoMail->Subject = $nekoEmailSubject;
                $nekoMail->Body    = $nekoEmailStyleSheet.$nekoEmailContentHtml;
                $nekoMail->AltBody = $nekoEmailContentNoHtml;
                //Char Set
                $nekoMail->CharSet = 'UTF-8';
                $nekoMail->Encoding = 'base64';
                //Mensaje Enviado
                $nekoMail->send();
                echo 'Mensaje Enviado';
                $nekoMail->SmtpClose();
            } catch (Exception $e)
            {
                echo "El Mensaje no se Pudo Enviar. Mailer Error: {$nekoMail->ErrorInfo}";
            }
            //================ Fin Correo Electrónico ========================
            break;
        case "NEKOPROC-0001":
            //Proceso Creación Usuario
            //Ultimo Número de la Secuencia
            $nekoSelectLastIdNumber=$nekoConnectDB->GetOne("SELECT MAX(codigousuario) FROM usuarios ");
            if($nekoSelectLastIdNumber==NULL OR $nekoSelectLastIdNumber=="0")
            {
                $nekoSelectNextIdNumber=1;
            }
            else
            {
                $nekoSelectNextIdNumber=$nekoSelectLastIdNumber+1;
            }
            $nekoUserName=$_POST["nekoUserNameCreation"];
            $nekoPassword=base64_encode($_POST["nekoUserPasswordCreation"]);
            $nekoEmail=$_POST["nekoUserEmailConfirmation"];
            $nekoRecoverQuestionCode=$_POST["nekoRecoverQuestion"];
            $nekoRecoverAnswer=$_POST["nekoRecoverQuestionAnswer"];
            $nekoAssociatedCode=$_POST["nekoAssociatedCode"];
            //Obtener el Codigo de la Empresa
            $nekoEnterpriseCode=$nekoConnectDB->GetOne("SELECT codigoempresa FROM empresas WHERE codigoasociado='$nekoAssociatedCode' ");
            $nekoEnterpriseCountryCode=$nekoConnectDB->GetOne("SELECT codigopais FROM empresas WHERE codigoasociado='$nekoAssociatedCode' ");
            $nekoEnterpriseName=$nekoConnectDB->GetOne("SELECT nombreempresa FROM empresas WHERE codigoasociado='$nekoAssociatedCode' ");
            $nekoRecoverQuestion=$nekoConnectDB->GetOne("SELECT denominacion FROM condicionadores WHERE codigocondicionador='$nekoRecoverQuestionCode' ");
            if($nekoEnterpriseName=="INDEPENDIENTE")
            {
                $nekoEnterpriseName=$nekoConnectDB->GetOne("SELECT nombreContacto FROM empresas WHERE codigoasociado='$nekoAssociatedCode' ");
            }
            //Obtener Color Asignado
            $nekoAsignatedColorCode=nekoRandomColorGenerator();
            //Ejecutar
            $nekoSeekSql=$nekoConnectDB->Execute("INSERT INTO usuarios VALUES('$nekoSelectNextIdNumber','$nekoUserName','$nekoEmail','$nekoPassword','4','1') ");
            //Crear Perfil de Detalles
            $nekoSelectLastIdNumberDetail=$nekoConnectDB->GetOne("SELECT MAX(codigodetallesusuario) FROM detallesUsuario ");
            if($nekoSelectLastIdNumberDetail==NULL OR $nekoSelectLastIdNumberDetail=="0")
            {
                $nekoSelectNextIdNumberDetail=1;
            }
            else
            {
                $nekoSelectNextIdNumberDetail=$nekoSelectLastIdNumberDetail+1;
            }
            $nekoSeekSql=$nekoConnectDB->Execute("INSERT INTO detallesUsuario VALUES('$nekoSelectNextIdNumberDetail','$nekoSelectNextIdNumber','$nekoRecoverQuestionCode','$nekoRecoverAnswer','$nekoAsignatedColorCode','','','0000-00-00','0','$nekoEnterpriseCode','$nekoEnterpriseCountryCode') ");
            //Statistics
            include_once("./registerStatistics.php");
            //================ Enviar Correo Electrónico ========================
            //================  Contenido del Mail ================ 
            $nekoEmailMainContent='
            <h4>Usted se ha registrado exitosamente en el Help Desk en la fecha '.$nekoDateCompleteDetail.' bajo los siguientes detalles:</h4>
            <p>
              <ul>
                <li><strong>Empresa: </strong> '.$nekoEnterpriseName.'</li>
                <li><strong>Nombre de Usuario: </strong> '.$nekoUserName.'</li>
                <li><strong>Contraseña: </strong> '.base64_decode($nekoPassword).'</li>
                <li><strong>Correo: </strong> '.$nekoEmail.'</li>
                <li><strong>Pregunta de Seguridad: </strong> '.$nekoRecoverQuestion.'</li>
                <li><strong>Respuesta: </strong> '.$nekoRecoverAnswer.'</li>
                <li><strong>Color (Para notificar de área segura): <span style="padding:10px;background-color:'.$nekoAsignatedColorCode.';">&nbsp;&nbsp;&nbsp;&nbsp;</span></li>
              </ul>
              Proceda a <a href="'.$nekoCompanyClass::nekoEnterpriseSupportWebSite.'">ingresar</a> al Help Desk para iniciar el proceso de Atención General. 
            </p>';
            $nekoEmailMainContentNoHtml='
            Usted se ha registrado exitosamente en el Help Desk en la fecha '.$nekoDateCompleteDetail.' bajo los siguientes detalles:
                Empresa: '.$nekoEnterpriseName.'
                Nombre de Usuario: </strong> '.$nekoUserName.'
                Contraseña: </strong> '.base64_decode($nekoPassword).'
                Correo: '.$nekoEmail.'
                Pregunta de Seguridad: '.$nekoRecoverQuestion.'
                Respuesta: '.$nekoRecoverAnswer.'
            
            Proceda Ingresar al Help Desk para iniciar el proceso de Atención General.';
            //================  Contenido del Mail ================ 
            //Envio
            try
            {
                //Llamar a Mailer
                include("./nekoMailer.php");
                //Variables
                $nekoEmailSubject='Creación de Usuario';
                //Incluir Variables
                include("../template/emails/emailStyleSheet.php");
                include("../template/emails/mainEmailTemplate.php"); //Template a Usar
                //Recipients
                $nekoMail->setFrom("dearaya91@gmail.com", "ColectivAndo",);
                $nekoMail->addAddress($nekoEmail);     //Add a recipient
                $nekoMail->addReplyTo("dearaya91@gmail.com", "ColectivAndo",);
                $nekoMail->addBCC('ldm@andalusacorp.com'); //Carbon Copy
                //$nekoMail->addBCC('bcc@example.com'); //Carbon Copy Hidden
                //Attachments
                //$nekoMail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
                $nekoMail->AddEmbeddedImage('../assets/img/basic/logo-colored.svg', 'LOGO-ANDALUSA');    //Image Embedded
                //Contenido
                $nekoMail->isHTML(true);                                  //Set email format to HTML
                $nekoMail->Subject = $nekoEmailSubject;
                $nekoMail->Body    = $nekoEmailStyleSheet.$nekoEmailContentHtml;
                $nekoMail->AltBody = $nekoEmailContentNoHtml;
                //Char Set
                $nekoMail->CharSet = 'UTF-8';
                $nekoMail->Encoding = 'base64';
                //Mensaje Enviado
                $nekoMail->send();
                echo 'Mensaje Enviado';
                $nekoMail->SmtpClose();
            } catch (Exception $e)
            {
                echo "El Mensaje no se Pudo Enviar. Mailer Error: {$nekoMail->ErrorInfo}";
            }
            //================ Fin Correo Electrónico ========================
            break;
        case "NEKOAJAX-1001":
            //Proceso BUSCA si existe Usuario
            $nekoName=nekoStringUpperCase($_POST["nekoName"]);
            $nekoSeekSql=$nekoConnectDB->Execute("SELECT nombreusuario FROM usuarios WHERE UPPER(nombreusuario)='$nekoName' ");
            if($nekoSeekSql->RecordCount() > 0)
            {
                echo 'N';
            }
            else
            {
                echo 'Y';
            }
            break;
        case "NEKOAJAX-1002":
            //Proceso BUSCA si existe email
            $nekoEmail=strtolower($_POST["nekoEmail"]);
            $nekoSeekSql=$nekoConnectDB->Execute("SELECT correo FROM usuarios WHERE LOWER(correo)='$nekoEmail'");
            if($nekoSeekSql->RecordCount() > 0)
            {
                echo 'N';
            }
            else
            {
                echo 'Y';
            }
            break;
        case "NEKOAJAX-1003":
            //Proceso BUSCA si existe codigo asociado
            $nekoCode=$_POST["nekoCode"];
            $nekoSeekSql=$nekoConnectDB->Execute("SELECT codigoasociado FROM empresas WHERE codigoasociado='$nekoCode' ");
            if($nekoSeekSql->RecordCount() > 0)
            {
                echo 'Y';
            }
            else
            {
                echo 'N';
            }
            break;
        case "NEKOAJAX-1004":
            //Proceso BUSCA si existe Usuario
            $nekoName=nekoStringUpperCase($_POST["nekoName"]);
            $nekoSeekSql=$nekoConnectDB->Execute("SELECT nombreusuario FROM usuarios WHERE UPPER(nombreusuario)='$nekoName' ");
            if($nekoSeekSql->RecordCount() > 1)
            {
                echo 'N';
            }
            else
            {
                echo 'Y';
            }
            break;
        case "NEKOAJAX-1005":
            //Proceso BUSCA si existe email
            $nekoEmail=strtolower($_POST["nekoEmail"]);
            $nekoSeekSql=$nekoConnectDB->Execute("SELECT correo FROM usuarios WHERE LOWER(correo)='$nekoEmail'");
            if($nekoSeekSql->RecordCount() > 1)
            {
                echo 'N';
            }
            else
            {
                echo 'Y';
            }
            break;
        case "NEKOPROC-GENE":
            //Proceso GENERAL template
            break;
        default:
            //No existe el proceso, enviar 404
            header($_SERVER["SERVER_PROTOCOL"].'404 Not Found');
            break;
    }
?>