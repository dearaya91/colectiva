<?php
    /*
      mainEmailTemplate.php
    */
    //Ruta Relativa
    include("../template/emails/nekoEmailHeader.php");
    include("../template/emails/nekoEmailFooter.php");
    //HTML
    $nekoEmailContentHtml='
    <body>
    <!-- <style> -->
    <table class="body" data-made-with-foundation="">
      <tr>
        <td class="float-center" align="center" valign="top">
          <center data-parsed="">
            '.$nekoEmailHeaderHtml.'
            <table align="center" class="container body-border float-center">
              <tbody>
                <tr>
                  <td>
                    <table class="row">
                      <tbody>
                        <tr>
                          <th class="small-12 large-12 columns first last">
                            <table>
                              <tr>
                                <th>
                                  <table class="spacer">
                                    <tbody>
                                      <tr>
                                        <td height="32px" style="font-size:32px;line-height:32px;">&#xA0;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <center data-parsed=""> <img src="cid:LOGO-ANDALUSA" alt="LOGO-ANDALUSA" style="width:190px;" align="center" class="float-center"> </center>
                                  <table class="spacer">
                                    <tbody>
                                      <tr>
                                        <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  '.$nekoEmailMainContent.'
                                  '.$nekoEmailFooterHtml.'
                                </th>
                                <th class="expander"></th>
                              </tr>
                            </table>
                          </th>
                        </tr>
                      </tbody>
                    </table>
                    <table class="spacer">
                      <tbody>
                        <tr>
                          <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </center>
        </td>
      </tr>
    </table>
  </body>
    ';
    //No HTML
    $nekoEmailContentNoHtml='
    '.$nekoEmailHeaderNoHtml.'
    '.$nekoEmailMainContentNoHtml.'
    '.$nekoEmailFooterNoHtml.'
    ';
?>