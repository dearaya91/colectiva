<?php
    /*
        nekoEnvironment.php
    */

    //Variables en Entorno
    $nekoGateWayInterface=$_SERVER["GATEWAY_INTERFACE"]; //Que se Produjo CGI/1.1
    $nekoRemoteIP=$_SERVER["REMOTE_ADDR"]; //Dirección IP
    $nekoServerName=$_SERVER["SERVER_NAME"]; //Nombre del Servidor
    $nekoServerSoftware=$_SERVER["SERVER_SOFTWARE"]; //Software del Servidor
    $nekoServerProtocol=$_SERVER["SERVER_PROTOCOL"]; //Protocolo del Servidor
    $nekoRequestMethod=$_SERVER["REQUEST_METHOD"]; //Request por GET, POST, PUT, HEAD, ETC...
    $nekoQueryString=$_SERVER["QUERY_STRING"]; //Consulta la dirección de la pagina
    $nekoDocumentRoot=$_SERVER["DOCUMENT_ROOT"]; //Raiz del Documento
    $nekoHTTPAcceptHeader=$_SERVER["HTTP_ACCEPT"]; //Software del Servidor
    $nekoHTTPAcceptHeaderCharset=$_SERVER["HTTP_ACCEPT_CHARSET"]; //Software del Servidor
    $nekoHTTPAcceptHeaderLanguage=$_SERVER["HTTP_ACCEPT_LANGUAGE"]; //Software del Servidor
    $nekoHTTPAcceptConnection=$_SERVER["HTTP_CONNECTION"];
    $nekoHTTPHost=$_SERVER["HTTP_HOST"];
    $nekoHTTPSRequest=$_SERVER["HTTPS"]; //Request del HTTPS
    $nekoHTTPReferer=$_SERVER["HTTP_REFERER"];
    $nekoWebBrowserMotorRequest=$_SERVER["HTTP_USER_AGENT"]; //Por medio de que motor de browser envio la solicitud
    $nekoHostNameAddress=gethostbyaddr($_SERVER["REMOTE_ADDR"]); //El nombre del Host de esa IP
    $nekoRemotePortNumber=$_SERVER["REMOTE_PORT"]; //Puerto Remoto
    $nekoStringLocationScript=$_SERVER["SCRIPT_FILENAME"]; //Que pagina se empleo - Similar al URI PERO con toda la dirección desde la raiz
    $nekoServerAdminName=$_SERVER["SERVER_ADMIN"];
    $nekoServerPortNumber=$_SERVER["SERVER_PORT"]; //Puerto del Servidor
    $nekoServerSign=$_SERVER["SERVER_SIGNATURE"];
    $nekoScriptName=$_SERVER["SCRIPT_NAME"]; //Que pagina se empleo - Similar al URI
    $nekoRequestPageName=$_SERVER["REQUEST_URI"]; //Que pagina se empleo
    $nekoPageNameBase=basename($_SERVER["PHP_SELF"],".php"); //Nombre de la Pagina
?>