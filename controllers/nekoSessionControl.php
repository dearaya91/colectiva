<?php
    /*
        nekoSessionControl.php
    */
    session_start();
    //Inicia la sesión
    if( file_exists("./controllers/nekoFunctions.php") && is_readable("./controllers/nekoFunctions.php") && include("./controllers/nekoFunctions.php"))
    {
        include_once("./controllers/nekoFunctions.php");
    }
    else
    {
        include_once("./nekoFunctions.php");
    }
    if($_SESSION["nekoUserLogged"]!=true)
    {
        //Relocalizar
        header("Location: ../login.php?err=".base64_encode("x01"));
    }
    //Verificar las paginas que debe consultar dependiendo del rol
    if($nekoDocumentControllerAvailable!=true) //Que no sea el controlador de Ajax
    {
        if(!in_array($nekoPageNameBase,$_SESSION["nekoAllPagesArray"]))
        {
            if(!in_array($nekoPageNameBase,$_SESSION["nekoAllConfigPagesArray"]))
            {
                //Va a la página asignada con un Error
                header("Location: ./".$_SESSION["nekoUserMainPage"]."?err=".base64_encode("y01"));
            }
        }
    }
?>