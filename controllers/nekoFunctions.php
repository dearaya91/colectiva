<?php
    /*
    ========================================
    DOCUMENT TITLE: nekoFunctions.php
    Made by Fenrir
    Content: Omega Ticketer
    ===========================================
    */
    /*
    ===========================================
                    Functions
    ===========================================
    */
    //Ver si Existe
    if( file_exists("./controllers/nekoDB.php") && is_readable("./controllers/nekoDB.php") && include("./controllers/nekoDB.php"))
    {
        include("./controllers/nekoDB.php");
    }
    else
    {
        //Base de Datos
        include("./nekoDB.php");
    }
    //Update after, Which Language to use
    $nekoSetLanguageVar="es_ES";
    $nekoSetLanguageAndMoneyVar="es_ES@euro";
    $nekoSetLanguageThreeVar="esp";
    $nekoSetLanguageVarWithExtension="es_ES.UTF-8";
    $nekoLanguageSimpleSet="es";
    setlocale(LC_ALL,$nekoSetLanguageAndMoneyVar,$nekoSetLanguageVar,$nekoSetLanguageThreeVar);
    setlocale(LC_TIME, $nekoSetLanguageVarWithExtension);
    //All variables, functions and classes
    $nekoYearObtained=date('Y');
    $nekoCurrentDateTime=date('Y-m-d H:i:s');
    $nekoCurrentDate=date('Y-m-d');
    $nekoCurrentTime=date('H:i:s');
    $nekoCurrentDay=date('d');
    $nekoCurrentMonth=date('m');
    /*
    ===========================================
                    Time Names
    ===========================================
    */
    $nekoDateString = DateTime::createFromFormat("Y-m-d", $nekoCurrentDate);
    $nekoDateDayFullName = strftime("%A",$nekoDateString->getTimestamp());
    $nekoDateDayAbbreviatedName = strftime("%a",$nekoDateString->getTimestamp());
    $nekoDateMonthFullName = strftime("%B",$nekoDateString->getTimestamp());
    $nekoDateMonthAbbreviatedName = strftime("%b",$nekoDateString->getTimestamp());
    $nekoDateWeekSundays = strftime("%U",$nekoDateString->getTimestamp());
    $nekoDateWeekMondays = strftime("%W",$nekoDateString->getTimestamp());
    //Fecha Formateada con el detalle del dia, mes, año iconv("UTF-8", "ISO-8859-1", $text)
    $nekoDateCompleteDetail = strftime("%A, %d de %B del %Y", strtotime($nekoCurrentDate));
    /*
    ===========================================
                    Functions
    ===========================================
    */
    function nekoDateMonthsBetween($nekoStartDate, $nekoEndDate)
    {
        //TERMINO ADICIONAL
        $nekoDate1 = $nekoStartDate;
        $nekoDate2 = $nekoEndDate;
        $nekoDiff = abs(strtotime($nekoDate2) - strtotime($nekoDate1));
        $nekoTotalYears = floor($nekoDiff / (365*60*60*24));
        $nekoMonths = floor(($nekoDiff - $nekoTotalYears * 365*60*60*24) / (30*60*60*24));
        $nekoTotalDays = floor(($nekoDiff - $nekoTotalYears * 365*60*60*24 - $nekoMonths*30*60*60*24)/ (60*60*24));
        $nekoTotalMonths=(($nekoTotalYears * 12) + $nekoMonths);
        return $nekoTotalMonths;
    }
    function nekoLastDayofMonth($nekoDate)
    {
        $nekoD = new DateTime($nekoDate); 
        return $nekoD->format('Y-m-t');
    }
    function nekoRounder($nekoValue, $nekoPrecision)
    {
        $nekoPow = pow ( 10, $nekoPrecision );
        return ( ceil ( $nekoPow * $nekoValue ) + ceil ( $nekoPow * $nekoValue - ceil ( $nekoPow * $nekoValue ) ) ) / $nekoPow;
    }
    function nekoRandomColorGenerator()
    {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }
    //Cadena aleatoria
    function nekoRandomStringGenerator($length=10,$uc=TRUE,$n=TRUE,$sc=FALSE)
    {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '@#$%=*+-';
        if($length>0)
        {
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++)
            {
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }
    
        }
        return $rstr;
    }

    function nekoStringUpperCase($string, $encoding = 'UTF-8')
    {
        return mb_strtoupper(trim($string), $encoding);
    }

    function nekoStringLowerCase($string, $encoding = 'UTF-8')
    {
        return mb_strtolower(trim($string), $encoding);
    }

    function nekoFirstLetterUppercase($string, $encoding = 'UTF-8')
    {
        $strlen = mb_strlen(trim($string), $encoding);
        $firstChar = mb_substr(trim($string), 0, 1, $encoding);
        $then = mb_substr(trim($string), 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
    /*
    ===========================================
                    INCLUDES
    ===========================================
    */
    //Si no es página para consultar para el cliente
    if( file_exists("./controllers/nekoEnvironment.php") && is_readable("./controllers/nekoEnvironment.php") && include("./controllers/nekoEnvironment.php"))
    {
        include_once("./controllers/nekoEnvironment.php");
    }
    else
    {
        include_once("./nekoEnvironment.php");
    }
    /*
    ===========================================
            LOGIN - SESSION DESTROYER
    ===========================================
    */
    //Destruir Sesión solo para el Login
    if($nekoPageNameBase=="index" OR $nekoPageNameBase=="register" OR $nekoPageNameBase=="index")
    {
        //Deshacerse de la Sesión de una manera óptima cuando llegue a Login siempre
        session_start();
        session_unset();
        session_destroy();
        session_write_close();
        setcookie(session_name(),'',0,'/');
        session_regenerate_id(true);
    }
    /*
    ===========================================
                VARIABLES
    ===========================================
    */
    $nekoPageTitle="ColectivAndo";
    $nekoVersion="1.0";
?>