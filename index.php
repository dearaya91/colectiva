<?php
    /*
        index.php
    */
    include("./controllers/nekoFunctions.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            //Incluir Archivo
            include("./templates/nekoHeader.php");
        ?>
    </head>
    <body>
        <?php
            //Incluir Menu
            include("./templates/nekoMenu.php");
        ?>
        <!-- BANNER -->
        <section class="nekoMainBanner mbr-fullscreen mbr-parallax-background" id="header2-0" data-rv-view="1301">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
            </div>
            <div class="container align-center">
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-lg-10">
                        <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">LA CALLE ES NUESTRA</h1>
                        <p class="mbr-text pb-3 mbr-fonts-style display-4">
                            Viajemos libres y seguras.
                            <br>
                            Cuidémonos juntas.
                            <br>
                            Informémonos y construyamos.
                            <br>
                            Mapeando nuestra seguridad.
                        </p>
                        <div class="mbr-section-btn">
                            <a class="btn btn-md btn-white display-4" href="register.php">Registrate Ya</a>
                        </div>
                    </div>
                </div>
            </div>
            <img id="nekoImg" alt="ColectivAndo02-Logo" class="nekoImg animate__animated animate__fadeInUp" src="assets/images/avatars/2.png">
            <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
                <a href="#next">
                    <i class="fas fa-info-circle"></i>
                </a>
            </div>
        </section>
        <!-- BANNER -->
        <!-- SUBSECTION -->
        <section class="header3 nekoSubsection01" id="header3-1" data-rv-view="1304">
            <div class="container">
                <div class="media-container-row">
                    <div class="mbr-figure" style="width: 100%;">
                        <img class="animate__animated animate__fadeInUp" src="assets/images/Colectiva(5).png" alt="Image" title="" media-simple="true">
                    </div>
                    <div class="media-content">
                        <h2 class="animate__animated animate__fadeInUp mbr-section-title mbr-white pb-3 mbr-fonts-style display-1">
                            Colectivizando
                        </h2>
                        <div class="mbr-section-text mbr-white pb-3 ">
                            <p class="mbr-text mbr-fonts-style display-4">
                                Informémonos juntas sobre nuestros derechos, mecanismos de denuncia y respuestas institucionales para defendernos de la violencia machista en espacios públicos. 
                            </p>
                        </div>
                        <div class="mbr-section-btn"><a class="btn btn-md btn-black-outline display-4" href="register.php">Registrate Ya</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- SUBSECTION -->
        <!-- NOTICES -->
        <section id="slider">
            <div class="slider-content">
                <div class="image image-first" data-href="#head" data-click="0">
                    <div class="slider-item">
                        <img class="item-img-1" src="assets/images/slider/1.png" media-simple="true">
                        <img class="item-img-2" src="assets/images/slider/2.jpg" media-simple="true">
                    </div>
                    <span class="image-text display-2">Conversando y reconociendonos</span>
                </div>
                <div class="image" data-href="#head" data-click="0">
                    <div class="slider-item">
                        <img class="item-img-1" src="assets/images/slider/3.jpg" media-simple="true">
                        <img class="item-img-2" src="assets/images/slider/4.jpg" media-simple="true">
                    </div>
                    <span class="image-text display-2">Conversando y construyendo juntas</span>
                </div>
                <div class="image" data-href="#head" data-click="0">
                    <div class="slider-item">
                        <img class="item-img-1" src="assets/images/slider/5.jpg" media-simple="true">
                        <img class="item-img-2" src="assets/images/slider/6.jpg" media-simple="true">
                    </div>
                    <span class="image-text display-2">Conversando y reconociendo<br> nuestra historia de lucha</span>
                </div>
                <div class="image image-last" data-href="#head" data-click="0">
                    <div class="slider-item">
                        <img class="item-img-1" src="assets/images/slider/7.png" media-simple="true">
                        <img class="item-img-2" src="assets/images/slider/8.jpg" media-simple="true">
                    </div>
                    <span class="image-text display-2">Conversando por espacios<br> públicos libres de violencia</span>
                </div>
            </div>
            <!-- 
            <div class="slider-content">
                <div class="image image-first" data-href="#" data-click="0">
                    <div class="slider-item">
                        <img class="item-img-1"  src="assets/images/ross-sokolovski-115060-2000x1333.jpg" alt="Mobirise" title="" media-simple="true">
                        <div class="item-img-2">Hola</div>
                    </div>
                    <span class="image-text">Smoovall</span>
                </div>
                <div class="image" data-href="#" data-click="0">
                    <div class="slider-item">
                        <img src="assets/images/ross-sokolovski-115060-2000x1333.jpg" alt="Mobirise" title="" media-simple="true">
                        <div class="item-img-2" data-src="assets/images/aravind-kumar-380959-2000x1333.jpg"></div>
                    </div>
                    <span class="image-text">Smoovall</span>
                </div>
                <div class="image" data-href="#" data-click="0">
                    <div class="slider-item">
                        <div class="item-img-1" data-src="assets/images/ross-sokolovski-115060-2000x1333.jpg"></div>
                        <div class="item-img-2" data-src="assets/images/ross-sokolovski-115060-2000x1333.jpg"></div>
                    </div>
                    <span class="image-text">Smoovall</span>
                </div>
                <div class="image image-last" data-href="#" data-click="0">
                    <div class="slider-item">
                        <div class="item-img-1" data-src="assets/images/annie-spratt-251226-2000x1333.jpg"></div>
                        <div class="item-img-2" data-src="assets/images/annie-spratt-251226-2000x1333.jpg"></div>
                    </div>
                    <span class="image-text">Smoovall</span>
                </div>
            </div> -->
        </section>
        <!-- NOTICES -->
        <!-- NOTICES -->
        <section class="features3 nekoNotices" id="features3-7" data-rv-view="1310">
            <div class="container">
                <div class="media-container-row">
                    <div class="card p-3 col-12 col-md-6 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <img src="assets/images/talk2.jpg" alt="Mobirise" title="" media-simple="true">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-7"><strong>
                                    Las Colectivas</strong></h4>
                                <p class="mbr-text mbr-fonts-style display-7">
                                    Organicemos nuestras comunidades y articulemos nuestras redes para continuar construyendo espacios públicos sin desigualdad de género </p>
                            </div>
                            <div class="mbr-section-btn text-center">
                                <a href="login.php" class="btn btn-black display-4">
                                    Ingresa
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card p-3 col-12 col-md-6 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <img src="assets/images/parada.jpg" alt="Mobirise" title="" media-simple="true">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-7"><strong>
                                    ¿Vas a iniciar tu recorrido y te sientes insegura?</strong></h4>
                                <p class="mbr-text mbr-fonts-style display-7">
                                Pódes compartir tu ruta en tiempo real con nuestras Guardianas
								</p>
                            </div>
                            <div class="mbr-section-btn text-center">
                                <a href="login.php" class="btn btn-black display-4">
                                    Ingresa
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card p-3 col-12 col-md-6 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <img src="assets/images/Colectiva(12).png" alt="Mobirise" title="" media-simple="true">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-7"><strong> ¿Sabías que existe una ley contra el acoso callejero?</strong></h4>
                                <p class="mbr-text mbr-fonts-style display-7">
                                    Conozcamos sobre qué es acoso callejero, qué contempla la ley y cómo podés denunciarlo. El acoso nunca es tu culpa.</p>
                            </div>
                            <div class="mbr-section-btn text-center">}
                                <a href="login.php" class="btn btn-black display-4">
                                    Ingresa
                                </a>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </section>
        <!-- NOTICES -->
        <!-- CREDITS -->
        <section class="nekoCredits" id="social-buttons2-c" data-rv-view="1313">
            <div class="container">
                <div class="media-container-row">
                    <div class="col-md-8 align-center">
                        <h2 class="pb-3 mbr-fonts-style display-2">
                            ¡SIGUENOS EN NUESTRAS REDES!
                        </h2>
                        <div class="social-list pl-0 mb-0">
                            <a href="https://twitter.com/" target="_blank">
                                <span class="px-2 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                            </a>
                            <a href="https://www.facebook.com/" target="_blank">
                                <span class="px-2 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                            </a>
                            <a href="https://instagram.com/" target="_blank">
                                <span class="px-2 socicon-instagram socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                            </a>
                            <a href="https://www.youtube.com" target="_blank">
                                
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- CREDITS -->
        <!-- FOOTER -->
        <?php
            include("./templates/nekoFooter.php");
        ?>
        <!-- FOOTER -->
        <?php
            include("./templates/nekoScripts.php")
        ?>
    </body>
</html>